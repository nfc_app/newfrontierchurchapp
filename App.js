import React from 'react';

import { Provider } from 'react-redux';
import AppNavigation from '@navigation/AppNavigation';

import { store } from '@store';

const App = () => (
  <Provider store={store}>
    <AppNavigation screenProps={store} />
  </Provider>
);

export default App;
