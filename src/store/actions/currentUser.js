export const currentUserSetUser = currentUser => ({
  type: 'CURRENT_USER_SET',
  currentUser,
});

export const setSurveyData = surveyDataItem => ({
  type: 'SET_SURVEY_DATA',
  surveyDataItem,
});
