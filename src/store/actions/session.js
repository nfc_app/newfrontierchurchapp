// Currently, this will drive the 'resources' and 'location' reducers as well.
export const sessionSetLanguage = langVal => ({
  type: 'SESSION_SET_LANG',
  langVal,
});
