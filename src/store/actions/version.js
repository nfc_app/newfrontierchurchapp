import { appConstants } from '@resources/Constants';
import { getNextSunday } from '@resources/Utils';

export const getVersion = () => ({
  type: 'GET_VERSION',
});

export const getVersionSuccess = data => ({
  type: 'GET_VERSION_SUCCESS',
  data,
});

export const getVersionFail = () => ({
  type: 'GET_VERSION_FAIL',
});

export const getVersionSetNextFetchDate = data => ({
  type: 'GET_VERSION_SET_NEXT_FETCH_DATE',
  data,
});

function fetchVersion() {
  return dispatch => {
    // This currently doesn't get caught by any reducers
    dispatch(getVersion());
    return fetch(`${appConstants.serverAddr}/v1/version`)
      .then(response => response.json())
      .then(json => {
        if (json) {
          dispatch(getVersionSetNextFetchDate(getNextSunday()));
          dispatch(getVersionSuccess(json));
        }
      });
  };
}

function shouldFetchVersion(state) {
  const version = state.version.number;
  const nextFetchDate = state.version.nextFetchDate;
  const updateRequired = state.version.updateRequired;
  const date = new Date();
  if (!updateRequired && (!version || date > nextFetchDate)) {
    return true;
  }
  return false;
}

export function fetchVersionIfNeeded() {
  return (dispatch, getState) => {
    if (true || shouldFetchVersion(getState())) {
      return dispatch(fetchVersion());
    }
    return Promise.resolve();
  };
}
