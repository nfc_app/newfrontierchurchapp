import { appConstants } from '@resources/Constants';
import { getNextSunday } from '@resources/Utils';

export const getAnnouncement = () => ({
  type: 'GET_ANNOUNCEMENT',
});

export const getAnnouncementSuccess = data => ({
  type: 'GET_ANNOUNCEMENT_SUCCESS',
  data,
});

export const getAnnouncementFail = () => ({
  type: 'GET_ANNOUNCEMENT_FAIL',
});

export const getAnnouncementSetNextFetchDate = data => ({
  type: 'GET_ANNOUNCEMENT_SET_NEXT_FETCH_DATE',
  data,
});

function fetchAnnouncement() {
  return dispatch => {
    // This currently doesn't get caught by any reducers
    dispatch(getAnnouncement());
    // For development, this needs to be changed
    return fetch(`${appConstants.serverAddr}/v1/announcement?date=latest`)
      .then(response => response.json())
      .then(json => {
        if (json) {
          dispatch(getAnnouncementSetNextFetchDate(getNextSunday()));
          dispatch(getAnnouncementSuccess(json));
        }
      });
  };
}

function shouldFetchAnnouncement(state) {
  const announcement = state.announcement.list;
  const date = new Date();
  const nextFetchDate = state.announcement.nextFetchDate;
  if (!announcement || announcement.length === 0 || date > nextFetchDate) {
    return true;
  }
  return false;
}

/**
 * @param force used for scroll-down refresh: will fetch QT from server regardless
 */
export function fetchAnnouncementIfNeeded(force) {
  return (dispatch, getState) => {
    if (true || force || shouldFetchAnnouncement(getState())) {
      return dispatch(fetchAnnouncement());
    }
    return Promise.resolve();
  };
}
