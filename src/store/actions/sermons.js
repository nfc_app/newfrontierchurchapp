import { appConstants } from '@resources/Constants';

export const getSermonInfos = () => ({
  type: 'GET_SERMON_INFOS',
});

export const getSermonInfosSuccess = data => ({
  type: 'GET_SERMON_INFOS_SUCCESS',
  data,
});

export const getSermonInfosFail = error => ({
  type: 'GET_SERMON_INFOS_FAIL',
  error,
});

export const getSermonSetFetchTime = data => ({
  type: 'GET_SERMON_SET_FETCH_TIME',
  data,
});

// Thunks (Middleware)
function fetchSermonInfos() {
  return dispatch => {
    // This currently doesn't get caught by any reducers
    dispatch(getSermonInfos());
    return fetch(`${appConstants.serverAddr}/v1/sermon?date=latest`)
      .then(response => response.json())
      .then(json => {
        const retVal = {
          ko: [],
          en: [],
        };
        if (json) {
          json.map(sermon => {
            ['ko', 'en'].map(lang => {
              const languageBody = sermon[lang];
              if (languageBody) {
                const info = {
                  date: sermon.date,
                  url: sermon.url,
                  title: languageBody.title,
                  type: languageBody.type,
                  who: languageBody.who,
                };
                retVal[lang].push(info);
              }
            });
          });
          const date = new Date();
          dispatch(getSermonSetFetchTime(date.getTime()));
          dispatch(getSermonInfosSuccess(retVal));
        }
      });
  };
}

function shouldFetchSermons(state) {
  const sermonInfos = state.sermons.sermonInfos;
  const date = new Date();
  const stateFetchTimeDiff = date.getTime() - state.stateFetchTime;
  if (!sermonInfos || sermonInfos.length === 0 || stateFetchTimeDiff > appConstants.timeToFetch) {
    return true;
    // } else if (qts.isFetching) {
    //   return false
  }
  return false;
}

export function fetchSermonsIfNeeded(force) {
  // Note that the function also receives getState()
  // which lets you choose what to dispatch next.

  // This is useful for avoiding a network request if
  // a cached value is already available.

  return (dispatch, getState) => {
    if (true || force || shouldFetchSermons(getState())) {
      // Dispatch a thunk from thunk!
      return dispatch(fetchSermonInfos());
    }
    // Let the calling code know there's nothing to wait for.
    return Promise.resolve();
  };
}
