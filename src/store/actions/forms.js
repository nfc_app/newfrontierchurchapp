import { appConstants } from '@resources/Constants';

export const getApplicationFormInfos = () => ({
  type: 'GET_APPLICATION_INFOS',
});

export const getApplicationFormInfosSuccess = data => ({
  type: 'GET_APPLICATION_INFOS_SUCCESS',
  data,
});

export const getApplicationFormInfosFail = error => ({
  type: 'GET_APPLICATION_INFOS_FAIL',
  error,
});

export const getApplicationSetFetchTime = data => ({
  type: 'GET_APPLICATION_SET_FETCH_TIME',
  data,
});

// Thunks (Middleware)
function fetchApplicationFormInfos() {
  return dispatch => {
    // This currently doesn't get caught by any reducers
    dispatch(getApplicationFormInfos());
    return fetch(`${appConstants.serverAddr}/v1/application?date=latest`)
      .then(response => response.json())
      .then(json => {
        const retVal = {
          ko: [],
          en: [],
        };
        if (json) {
          json.map(application => {
            ['ko', 'en'].map(lang => {
              const languageBody = application[lang];
              if (languageBody) {
                const info = {
                  title: languageBody.title,
                  url: languageBody.URL,
                  content: languageBody.content,
                };
                retVal[lang].push(info);
              }
            });
          });
          const date = new Date();
          dispatch(getApplicationSetFetchTime(date.getTime()));
          dispatch(getApplicationFormInfosSuccess(retVal));
        }
      });
  };
}

function shouldFetchForms(state) {
  const formInfos = state.forms.applicationFormInfos;
  const date = new Date();
  const stateFetchTimeDiff = date.getTime() - state.stateFetchTime;
  if (!formInfos || formInfos.length === 0 || stateFetchTimeDiff > appConstants.timeToFetch) {
    return true;
    // } else if (qts.isFetching) {
    //   return false
  }
  return false;
  // return qts.didInvalidate;
}

export function fetchFormsIfNeeded(force) {
  // Note that the function also receives getState()
  // which lets you choose what to dispatch next.

  // This is useful for avoiding a network request if
  // a cached value is already available.

  return (dispatch, getState) => {
    if (true || force || shouldFetchForms(getState())) {
      // Dispatch a thunk from thunk!
      return dispatch(fetchApplicationFormInfos());
    }
    // Let the calling code know there's nothing to wait for.
    return Promise.resolve();
  };
}
