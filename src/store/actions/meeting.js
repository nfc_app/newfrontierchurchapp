import { appConstants } from '@resources/Constants';
import { getNextSunday } from '@resources/Utils';

export const getMeeting = () => ({
  type: 'GET_MEETING',
});

export const getMeetingSuccess = data => ({
  type: 'GET_MEETING_SUCCESS',
  data,
});

export const getMeetingFail = () => ({
  type: 'GET_MEETING_FAIL',
});

export const getMeetingSetNextFetchDate = data => ({
  type: 'GET_MEETING_SET_NEXT_FETCH_DATE',
  data,
});

function fetchMeeting() {
  return dispatch => {
    // This currently doesn't get caught by any reducers
    dispatch(getMeeting());
    return fetch(`${appConstants.serverAddr}v1/meeting?date=latest`)
      .then(response => response.json())
      .then(json => {
        if (json) {
          dispatch(getMeetingSetNextFetchDate(getNextSunday()));
          dispatch(getMeetingSuccess(json));
        }
      });
  };
}

function shouldFetchMeeting(state) {
  const meeting = state.meeting.list;
  const date = new Date();
  const nextFetchDate = state.meeting.nextFetchDate;
  if (!meeting || meeting.length === 0 || date > nextFetchDate) {
    return true;
  }
  return false;
}

export function fetchMeetingIfNeeded(force) {
  return (dispatch, getState) => {
    if (true || force || shouldFetchMeeting(getState())) {
      return dispatch(fetchMeeting());
    }
    return Promise.resolve();
  };
}
