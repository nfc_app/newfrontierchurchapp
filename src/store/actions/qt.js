import { appConstants } from '@resources/Constants';
import { getNextSunday } from '@resources/Utils';

export const getQt = () => ({
  type: 'GET_QT',
});

export const getQtContentSuccess = data => ({
  type: 'GET_QT_CONTENT_SUCCESS',
  data,
});

export const getQtInfoSuccess = data => ({
  type: 'GET_QT_INFO_SUCCESS',
  data,
});

export const getQtFail = () => ({
  type: 'GET_QT_FAIL',
});

export const qtIndexSet = value => ({
  type: 'SET_QT_INDEX',
  value,
});

export const qtIndexChange = value => ({
  type: 'CHANGE_QT_INDEX',
  value,
});

export const getQtSetNextFetchDate = data => ({
  type: 'GET_QT_SET_NEXT_FETCH_DATE',
  data,
});

function generateQtTitle(lang, isMainText, data) {
  let mainText = '';
  if (isMainText) {
    mainText = (lang == 'ko') ? '*순모임 본문' : '*Sunday Bible Study';
  }
  return `${data.book} ${data.chapter}:${data.verse} ${mainText}`;
}

// Thunks (Middleware)
function fetchQt() {
  return dispatch => {
    // This currently doesn't get caught by any reducers
    dispatch(getQt());
    return fetch(`${appConstants.serverAddr}/v1/qt?date=latest`)
      .then(response => response.json())
      .then(json => {
        if (json) {
          const startDate = json.dateRange;
          const mainTextIndex = json.mainText;
          const allLangRetVal = {};
          Object.keys(json.script).forEach(lang => {
            const body = json.script[lang];
            const langBody = [];
            for (const key in body) {
              const isMainText = key == mainTextIndex;
              const temp = {
                qtMetaData: {
                  dayOfWeek: body[key].day,
                  date: body[key].date,
                  qtTitle: generateQtTitle(lang, isMainText, body[key])
                },
                verses: [...body[key].word],
              };
              langBody.push(temp);
            }
            allLangRetVal[lang] = langBody;
          });
          dispatch(getQtSetNextFetchDate(getNextSunday()));
          dispatch(getQtContentSuccess(allLangRetVal));
          dispatch(getQtInfoSuccess(startDate));
        }
      });
  };
}

function shouldFetchQt(state) {
  const qts = state.qt.qtContent;
  const date = new Date();
  const nextFetchDate = state.qt.nextFetchDate;
  if (!qts || (Object.keys(qts).length === 0 && qts.constructor === Object) || date > nextFetchDate) {
    return true;
    // } else if (qts.isFetching) {
    //   return false
  }
  return false;
}

/**
 * @param force used for scroll-down refresh: will fetch QT from server regardless
 */
export function fetchQtsIfNeeded(force) {
  // Note that the function also receives getState()
  // which lets you choose what to dispatch next.

  // This is useful for avoiding a network request if
  // a cached value is already available.

  return (dispatch, getState) => {
    if (true || force || shouldFetchQt(getState())) {
      // Dispatch a thunk from thunk!
      return dispatch(fetchQt());
    }
    // Let the calling code know there's nothing to wait for.
    return Promise.resolve();
  };
}
