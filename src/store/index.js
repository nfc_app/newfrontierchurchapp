import { compose, combineReducers, createStore, applyMiddleware } from 'redux';
import { AsyncStorage } from 'react-native';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';
import { offline } from '@redux-offline/redux-offline';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults';

import about from './reducers/about';
import qt from './reducers/qt';
import session from './reducers/session';
import location from './reducers/location';
import forms from './reducers/forms';
import sermons from './reducers/sermons';
import announcement from './reducers/announcement';
import meeting from './reducers/meeting';
import nextgen from './reducers/nextgen';
import uiText from './reducers/uiText';
import currentUser from './reducers/currentUser';
import version from './reducers/version';

// import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
// import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';

// const pReducer = persistReducer(persistConfig, rootReducer);
// export const store = createStore(pReducer, applyMiddleware(thunkMiddleware));
// export const persistor = persistStore(store);

// const persistConfig = {
//   key: 'root',
//   storage,
//   stateReconciler: autoMergeLevel2,
//   // whitelist: [],
//   blacklist: ['uiText', 'about'],
// };

const rootReducer = combineReducers({
  about,
  uiText,
  qt,
  session,
  location,
  forms,
  sermons,
  announcement,
  meeting,
  nextgen,
  currentUser,
  version,
});

const config = {
  ...offlineConfig,
  persistCallback: () => console.log('Rehydratation complete'),
  persistOptions: {
    storage: AsyncStorage,
    whitelist: ['qt', 'session', 'forms', 'sermons', 'announcement', 'meeting'],
    debounce: 300,
  },
  rehydrate: true
}

export const store = createStore(
  rootReducer,
  undefined,
  compose(
    applyMiddleware(thunkMiddleware, logger),
    offline(config)
  )
);
