import { getVersionSuccess, getVersionSetNextFetchDate } from '@actions/version';

const initialState = {
  nextFetchDate: null,
  number: null,
};

const versionReducer = (state = initialState, action) => {
  switch (action.type) {
    case getVersionSuccess().type:
      return {
        ...state,
        number: action.data,
      };
    case getVersionSetNextFetchDate().type:
      return {
        ...state,
        nextFetchDate: action.data,
      };
    default:
      return state;
  }
};

export default versionReducer;
