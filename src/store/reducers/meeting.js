import { getMeetingSuccess, getMeetingSetNextFetchDate } from '@actions/meeting';

const initialState = {
  nextFetchDate: 0,
  list: [],
};

const meetingReducer = (state = initialState, action) => {
  switch (action.type) {
    case getMeetingSuccess().type:
      return Object.assign({}, state, {
        list: action.data,
      });
    case getMeetingSetNextFetchDate().type:
      return Object.assign({}, state, {
        nextFetchDate: action.data,
      });
    default:
      return state;
  }
};

export default meetingReducer;
