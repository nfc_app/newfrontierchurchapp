// clear this out once we have actual data flowing, this is working like mock data for now
/*
{
  qtMetaData: {
    dayOfWeek: "Monday",
    date: "1/1",
    qtTitle: "Genesis 1:1-2"
  },
  verses: [
    {
      verse: 1,
      body: "In the beginning, God created the heavens and the earth."
    },
    {
      verse: 2,
      body: "And the earth was without form, and void, and darkness was upon the face of the deep."
    }
  ]
}
*/
import { getQtContentSuccess, getQtInfoSuccess, getQtSetNextFetchDate, qtIndexChange, qtIndexSet } from '@actions/qt';

const qtInitialState = {
  nextFetchDate: null,
  selectionIndex: 0,
  qtContent: {},
  startingDate: '',
};

const qtReducer = (state = qtInitialState, action) => {
  switch (action.type) {
    case qtIndexSet().type:
      return Object.assign({}, state, {
        selectionIndex: action.value,
      });
    case qtIndexChange().type:
      const newValue = state.selectionIndex + action.value;
      if (newValue < 0 || newValue >= 7) {
        return state;
      }
      return Object.assign({}, state, {
        selectionIndex: newValue,
      });

    case getQtContentSuccess().type:
      return Object.assign({}, state, {
        qtContent: action.data,
      });
    case getQtInfoSuccess().type:
      return Object.assign({}, state, {
        startingDate: action.data,
      });
    case getQtSetNextFetchDate().type:
      return Object.assign({}, state, {
        nextFetchDate: action.data,
      });
    default:
      return state;
  }
};

export default qtReducer;
