// Application Forms State

import { getApplicationFormInfosSuccess, getApplicationSetFetchTime } from "@actions/forms";

const applicationFormInfosState = {
  stateFetchTime: 0,
  applicationFormInfos: [],
};

const formsReducer = (state = applicationFormInfosState, action) => {
  switch (action.type) {
    case getApplicationFormInfosSuccess().type:
      return Object.assign({}, state, {
        applicationFormInfos: action.data,
      });
    case getApplicationSetFetchTime().type:
      return Object.assign({}, state, {
        stateFetchTime: action.data,
      });
    default:
      return state;
  }
};

export default formsReducer;
