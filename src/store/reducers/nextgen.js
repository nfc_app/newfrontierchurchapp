// Services and Location "State"
// Currently, services and locations are static data, but
// giving it a state in the store creates room for expanding as non-static data

import { sundaySchool, koreaSchool } from '@resources/nextgen/NextGen';

const nextGenInitialState = {
  sunday: sundaySchool,
  korea: koreaSchool,
};

const nextGenReducer = (state = nextGenInitialState, action) => {
  switch (action.type) {
    default:
      return Object.assign({}, state);
  }
};

export default nextGenReducer;
