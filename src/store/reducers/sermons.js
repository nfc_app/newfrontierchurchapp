// Sermon List State

import { getSermonInfosSuccess, getSermonSetFetchTime } from '@actions/sermons';

const sermonInfosState = {
  stateFetchTime: 0,
  sermonInfos: [],
};

const sermonReducer = (state = sermonInfosState, action) => {
  switch (action.type) {
    case getSermonInfosSuccess().type:
      return Object.assign({}, state, {
        sermonInfos: action.data,
      });
    case getSermonSetFetchTime().type:
      return Object.assign({}, state, {
        stateFetchTime: action.data,
      });
    default:
      return state;
  }
};

export default sermonReducer;
