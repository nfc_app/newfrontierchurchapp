import { sessionSetLanguage } from '@actions/session';
import { appConstants } from '@resources/Constants';

const sessionInitialState = {
  lang: appConstants.defaultLang,
};

const sessionReducer = (state = sessionInitialState, action) => {
  switch (action.type) {
    case sessionSetLanguage().type:
      return Object.assign({}, state, {
        lang: action.langVal,
      });
    default:
      return state;
  }
};

export default sessionReducer;
