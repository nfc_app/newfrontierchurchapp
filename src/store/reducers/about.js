// Services and Location "State"
// Currently, services and locations are static data, but
// giving it a state in the store creates room for expanding as non-static data

import { vision, history, pastors, leadership } from '@resources/about/About';

const aboutInitialState = {
  vision,
  history,
  pastors,
  leadership,
};

const aboutReducer = (state = aboutInitialState, action) => {
  switch (action.type) {
    default:
      return Object.assign({}, state);
  }
};

export default aboutReducer;
