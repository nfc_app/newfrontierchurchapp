import { currentUserSetUser, setSurveyData } from '@actions/currentUser';

const sessionInitialState = {
  currentUser: null,
  surveyData: {
    firstName: {
      en: "",
      ko: ""
    }
  },
};

const currentUserReducer = (state = sessionInitialState, action) => {
  switch (action.type) {
    case currentUserSetUser().type:
      return Object.assign({}, state, {
        currentUser: action.currentUser,
      });
    case setSurveyData().type:
      return {
        ...state,
        surveyData: {
          ...state.surveyData,
          ...action.surveyDataItem,
        },
      };

    default:
      return state;
  }
};

export default currentUserReducer;
