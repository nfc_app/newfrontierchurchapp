import { getAnnouncementSuccess, getAnnouncementSetNextFetchDate } from '@actions/announcement';

const initialState = {
  nextFetchDate: 0,
  list: [],
};

const announcementReducer = (state = initialState, action) => {
  switch (action.type) {
    case getAnnouncementSuccess().type:
      return Object.assign({}, state, {
        list: action.data,
      });
    case getAnnouncementSetNextFetchDate().type:
      return Object.assign({}, state, {
        nextFetchDate: action.data,
      });
    default:
      return state;
  }
};

export default announcementReducer;
