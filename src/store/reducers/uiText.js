import { sessionSetLanguage } from '@actions/session';
import { appConstants } from '@resources/Constants';
import uiTextMap from '@resources/uitext/UIText';

const uiTextInitialState = uiTextMap[appConstants.defaultLang];

const uiTextReducer = (state = uiTextInitialState, action) => {
  switch (action.type) {
    case sessionSetLanguage().type:
      return Object.assign({}, state, uiTextMap[action.langVal]);
    default:
      return state;
  }
};

export default uiTextReducer;
