import { Platform } from 'react-native';

export const COLOR_PRIMARY = '#e9128b';
export const COLOR_PRIMARY_DARK = '#be202f';
export const COLOR_BACKGROUND = '#ffffff';
export const COLOR_HYPERLINK = '#2F84EA';

export const COLOR_BLACK = '#000000';
export const COLOR_GREY = '#d8d8d8';
export const COLOR_GREY_LIGHT = 'rgba(29, 29, 38, 0.1)';
export const COLOR_GREY_LIGHTEST = 'rgba(29, 29, 38, 0.03)';
export const COLOR_GREY_DARK = '#a1a1a1';
export const COLOR_GREY_DARKEST = '#808080';

export const FONT_NORMAL = 'OpenSans-Regular';
export const FONT_BOLD = 'OpenSans-Bold';

export const FONT_SIZE_XLARGE = 36;
export const FONT_SIZE_LARGE = 20;
export const FONT_SIZE_MEDIUM = 18;
export const FONT_SIZE_SMALL = 14;
export const FONT_SIZE_XSMALL = 13;
export const FONT_SIZE_XXSMALL = 11;

export const TEXT_LINE_HEIGHT = 20;
export const DIVIDER_HEIGHT = 1;

export const SPACING_XXXSMALL = 4;
export const SPACING_XXSMALL = 8;
export const SPACING_XSMALL = 12;
export const SPACING_SMALL = 16;
export const SPACING_MEDIUM = 20;
export const SPACING_LARGE = 24;
export const SPACING_XLARGE = 28;
export const SPACING_XXLARGE = 32;
export const SPACING_XXXLARGE = 48;

export const SHADOW = {
  ...Platform.select({
    ios: {
      shadowColor: 'black',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
    },
    android: {
      // elevation: 5,
    },
  }),
};
