import {
  createSwitchNavigator,
  createStackNavigator,
  createDrawerNavigator,
} from 'react-navigation';

import Loading from '@pages/Loading';
import Update from '@pages/Update';

import Main from '@pages/main/Main';
import About from '@pages/about/About';
import Location from '@pages/location/Location';
import QtMain from '@pages/qt/QtMain';
import QtContent from '@pages/qt/QtContent';
import NextGeneration from '@pages/static_pages/NextGeneration';
import Prayer from '@pages/static_pages/Prayer';
import Offering from '@pages/static_pages/Offering';
import Contact from '@pages/static_pages/Contact';
import ApplicationPage from '@pages/application/ApplicationPage';
import SermonPage from '@pages/sermon/SermonPage';

import Settings from '@pages/settings/Settings';
import LanguageSetting from '@pages/settings/LanguageSetting';

import DrawerContainer from '@navigation/DrawerContainer';

const QtStack = createStackNavigator({
  QtMain,
  QtContent,
});

const SettingStack = createStackNavigator({
  Settings,
  LanguageSetting,
});

const DrawerStack = createDrawerNavigator(
  {
    main: { screen: Main },
    about: { screen: About },
    services: { screen: Location },
    qtpage: { screen: QtStack },
    application: { screen: ApplicationPage },
    sermon: { screen: SermonPage },
    nextgen: { screen: NextGeneration },
    prayer: { screen: Prayer },
    offering: { screen: Offering },
    contact: { screen: Contact },
    settings: { screen: SettingStack },
  },
  {
    contentComponent: DrawerContainer,
  }
);

const PrimaryNav = createSwitchNavigator(
  {
    Loading,
    Update,
    Main: DrawerStack,
  },
  {
    initialRouteName: 'Loading',
  }
);

export default PrimaryNav;
