import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, Platform, Text, View, Image, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  FONT_SIZE_MEDIUM,
  COLOR_PRIMARY,
  COLOR_PRIMARY_DARK,
  COLOR_BACKGROUND,
} from '@styles/Common';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export const UserBar = props => {
  const IMAGE_SETTINGS = require('@assets/images/icon_settings.png');
  const { surveyData, lang, navigation } = props;

  let nameVal = "";
  // Backward Compatibility: surveyData was introduced around 9/2018 and is populated during login.
  // if an app had logged in before this, it would not have surveyData, which then means that the UserBar, which
  // has the Settings Page access, which then has the "Log Out" button, won't be available, making it hard to
  // logout-login to make it available.
  // This issue potentially only applies to Me/Sumin/Jun, so eventually this can be removed.
  if (surveyData != null && surveyData.firstName != null && lang != null) {
    nameVal = surveyData.firstName[lang];
  } else {
    nameVal = "(No Name Data: Please Retry Login)";
  }
  return (
    <LinearGradient
      colors={[COLOR_PRIMARY_DARK, COLOR_PRIMARY]}
      start={{ x: 0.0, y: 1.0 }}
      end={{ x: 1.0, y: 1.0 }}
      style={styles.container}
    >
      <View style={{}}>
        <Text style={styles.drawerUserName}>{nameVal}</Text>
      </View>
      <TouchableOpacity
        style={styles.rightContainer}
        onPress={() => {
          navigation.navigate('settings');
        }}
      >
        <Image source={IMAGE_SETTINGS} />
      </TouchableOpacity>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: getStatusBarHeight(true) + 50,
    paddingTop: getStatusBarHeight(true),
    paddingBottom: Platform.OS === 'ios' ? 7 : 0,
    paddingLeft: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  drawerUserName: {
    color: COLOR_BACKGROUND,
    fontSize: FONT_SIZE_MEDIUM,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  rightContainer: {
    padding: 20,
  },
});

UserBar.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  surveyData: PropTypes.objectOf(PropTypes.any).isRequired,
  lang: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  lang: state.session.lang,
  surveyData: state.currentUser.surveyData,
});

export default connect(mapStateToProps)(UserBar);
