import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, Text, View } from 'react-native';
import { DrawerActions } from 'react-navigation';

import { COLOR_BLACK, COLOR_GREY_LIGHT, FONT_SIZE_MEDIUM, DIVIDER_HEIGHT } from '@styles/Common';
import UserBar from './UserBar';

const DrawerContainer = props => {
  const { lang, navText, navigation } = props;
  const screens = [];
  screens.push({ title: navText.home, routeName: 'main' });
  screens.push({ title: navText.about, routeName: 'about' });
  screens.push({ title: navText.location, routeName: 'services' });
  screens.push({ title: navText.qt, routeName: 'qtpage' });
  screens.push({ title: navText.forms, routeName: 'application' });
  if (lang === 'ko') {
    screens.push({ title: navText.sermon, routeName: 'sermon' });
  }
  screens.push({ title: navText.nextgen, routeName: 'nextgen' });
  screens.push({ title: navText.prayer, routeName: 'prayer' });
  screens.push({ title: navText.offering, routeName: 'offering' });
  screens.push({ title: navText.contact, routeName: 'contact' });

  return (
    <ScrollView style={styles.container}>
      <UserBar navigation={navigation} />
      {screens.map(item => (
        <View style={styles.drawerItemContainer} key={item.routeName}>
          <Text style={styles.drawerItem} onPress={() => onDrawerItemPress(navigation, item.routeName)}>
            {item.title}
          </Text>
        </View>
      ))}
    </ScrollView>
  );
};

function onDrawerItemPress(navigation, routeName) {
  navigation.dispatch(DrawerActions.closeDrawer());
  navigation.navigate(routeName)
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerItemContainer: {
    borderBottomColor: COLOR_GREY_LIGHT,
    borderBottomWidth: 0.5,
  },
  drawerItem: {
    color: COLOR_BLACK,
    fontSize: FONT_SIZE_MEDIUM,
    fontWeight: 'bold',
    paddingVertical: 20,
    paddingHorizontal: 30,
    textAlign: 'left',
  },
});

DrawerContainer.propTypes = {
  lang: PropTypes.string.isRequired,
  navText: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  lang: state.session.lang,
  navText: state.uiText.nav,
});

export default connect(mapStateToProps)(DrawerContainer);
