import React from 'react';
import PropTypes from 'prop-types';

import { ScrollableTabView } from '@valdio/react-native-scrollable-tabview';

import { COLOR_PRIMARY, COLOR_GREY_LIGHTEST } from '@styles/Common';

const TabBarView = props => (
  <ScrollableTabView
    collapsableBar={props.collapsibleComponent}
    tabBarActiveTextColor={COLOR_PRIMARY}
    tabBarBackgroundColor={COLOR_GREY_LIGHTEST}
    tabBarUnderlineStyle={{
      backgroundColor: COLOR_PRIMARY,
    }}
  >
    {props.tabs}
  </ScrollableTabView>
);

TabBarView.defaultProps = {
  collapsibleComponent: null,
};

TabBarView.propTypes = {
  collapsibleComponent: PropTypes.element,
  tabs: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default TabBarView;
