import React from 'react';
import { Image, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import {
  COLOR_BACKGROUND,
  COLOR_BLACK,
  COLOR_GREY,
  FONT_SIZE_SMALL,
  SPACING_MEDIUM,
  SPACING_LARGE,
  DIVIDER_HEIGHT,
} from '@styles/Common';

const RadioButtonRow = prop => (
  <TouchableWithoutFeedback onPress={prop.onPress}>
    <View style={styles.viewContainer}>
      <Text style={[styles.textContainer, prop.style]}>{prop.text}</Text>
      <Image source={prop.checked ? IMGAGE_RADIO_CHECKED : IMGAGE_RADIO_UNCHECKED} />
    </View>
  </TouchableWithoutFeedback>
);

const IMGAGE_RADIO_UNCHECKED = require('@assets/images/icon_radio_unchecked.png');
const IMGAGE_RADIO_CHECKED = require('@assets/images/icon_radio_checked.png');

const styles = StyleSheet.create({
  viewContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: COLOR_BACKGROUND,
    paddingVertical: SPACING_LARGE,
    paddingHorizontal: SPACING_MEDIUM,
    borderBottomColor: COLOR_GREY,
    borderBottomWidth: DIVIDER_HEIGHT,
  },
  textContainer: {
    fontSize: FONT_SIZE_SMALL,
    color: COLOR_BLACK,
    alignSelf: 'center',
  },
});

export default RadioButtonRow;
