import React from 'react';
import { Image, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import {
  COLOR_BACKGROUND,
  COLOR_BLACK,
  COLOR_GREY,
  FONT_SIZE_SMALL,
  SPACING_MEDIUM,
  SPACING_SMALL,
  DIVIDER_HEIGHT,
} from '@styles/Common';

const IMGAGE_ARROW_RIGHT = require('@assets/images/arrow_right.png');

const ListRow = prop => (
  <TouchableWithoutFeedback onPress={prop.onPress}>
    <View style={styles.viewContainer}>
      <View>
        <Text style={[styles.textContainer, prop.textStyle]}>{prop.text}</Text>
        {prop.subtext && <Text style={[styles.subtextContainer, prop.subtextStyle]}>{prop.subtext}</Text>}
      </View>
      {prop.showArrow && <Image source={IMGAGE_ARROW_RIGHT} style={styles.imgContainer} />}
    </View>
  </TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
  viewContainer: {
    backgroundColor: COLOR_BACKGROUND,
    paddingVertical: SPACING_SMALL,
    paddingHorizontal: SPACING_MEDIUM,
    borderBottomColor: COLOR_GREY,
    borderBottomWidth: DIVIDER_HEIGHT,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 70,
  },
  textContainer: {
    fontWeight: 'bold',
    fontSize: FONT_SIZE_SMALL,
    color: COLOR_BLACK,
  },
  subtextContainer: {
    fontSize: FONT_SIZE_SMALL,
    color: COLOR_BLACK,
  },
});

export default ListRow;
