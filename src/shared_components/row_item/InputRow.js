import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { COLOR_GREY, SPACING_MEDIUM, DIVIDER_HEIGHT } from '@styles/Common';

const InputRow = prop => (
  <View style={prop.containerStyle}>
    <TextInput style={styles.textInput} {...prop} />
  </View>
);

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    width: '100%',
    height: 40,
    color: 'black',
    borderBottomColor: COLOR_GREY,
    borderBottomWidth: DIVIDER_HEIGHT,
    marginVertical: SPACING_MEDIUM,
  },
});

export default InputRow;
