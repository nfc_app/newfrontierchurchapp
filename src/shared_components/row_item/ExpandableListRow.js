import React from 'react';
import { StyleSheet, Image, Text, TouchableWithoutFeedback, View } from 'react-native';
import {
  COLOR_BACKGROUND,
  COLOR_BLACK,
  COLOR_GREY_LIGHT,
  FONT_SIZE_SMALL,
  FONT_SIZE_XSMALL,
  SPACING_LARGE,
  SPACING_MEDIUM,
  SPACING_XXSMALL,
  SPACING_XXXSMALL,
  DIVIDER_HEIGHT,
} from '@styles/Common';

export default class ExpandableListRow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      maxNumOfLines: 1,
      arrowImage: require('@assets/images/arrow_down.png'),
      collapsed: true,
    };

    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    if (this.state.collapsed) {
      this.setState({
        maxNumOfLines: 0,
        arrowImage: require('@assets/images/arrow_up.png'),
        collapsed: false,
      });
    } else {
      this.setState({
        maxNumOfLines: 1,
        arrowImage: require('@assets/images/arrow_down.png'),
        collapsed: true,
      });
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={this.onPress}>
        <View style={styles.viewContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.textTitle}>{this.props.title}</Text>
            <Image source={this.state.arrowImage} />
          </View>
          <Text numberOfLines={this.state.maxNumOfLines} style={styles.textBody}>
            {this.props.body}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {
    backgroundColor: COLOR_BACKGROUND,
    paddingVertical: SPACING_LARGE,
    paddingHorizontal: SPACING_MEDIUM,
    borderBottomColor: COLOR_GREY_LIGHT,
    borderBottomWidth: DIVIDER_HEIGHT,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: SPACING_XXXSMALL,
  },
  textTitle: {
    fontSize: FONT_SIZE_SMALL,
    fontWeight: 'bold',
    color: COLOR_BLACK,
    marginRight: SPACING_XXSMALL,
  },
  textBody: {
    fontSize: FONT_SIZE_XSMALL,
    color: COLOR_BLACK,
  },
});
