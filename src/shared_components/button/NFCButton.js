import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableHighlight, View } from 'react-native';
import { Button } from 'react-native-elements';
import {
  COLOR_BACKGROUND,
  COLOR_PRIMARY,
  COLOR_GREY_DARK,
  FONT_SIZE_XSMALL,
  SPACING_SMALL
} from '@styles/Common';

const NFCButton = ({ text, onClick, fullWidth }) => (
  <View style={fullWidth ? styles.buttonContainer : {}}>
    <Button
      title={text}
      textStyle={styles.buttonTextContainer}
      buttonStyle={styles.buttonMainContainer}
      component={TouchableHighlight}
      underlayColor={COLOR_GREY_DARK}
      onPress={() => {
        onClick();
      }}
    />
  </View>
);

NFCButton.defaultProps = {
  fullWidth: true,
};

NFCButton.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  fullWidth: PropTypes.bool,
};

const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    margin: SPACING_SMALL,
  },
  buttonMainContainer: {
    backgroundColor: COLOR_BACKGROUND,
    paddingVertical: SPACING_SMALL,
    borderColor: COLOR_PRIMARY,
    borderWidth: 1,
  },
  buttonTextContainer: {
    color: COLOR_PRIMARY,
    fontSize: FONT_SIZE_XSMALL,
  },
});

export default NFCButton;
