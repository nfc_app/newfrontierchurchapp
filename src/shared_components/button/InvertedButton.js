import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { COLOR_BACKGROUND, FONT_SIZE_XSMALL, COLOR_PRIMARY, SPACING_SMALL } from '@styles/Common';

const InvertedButton = ({ text, onClick, fullWidth }) => (
  <Button
    title={text}
    textStyle={styles.buttonTextContainer}
    buttonStyle={styles.buttonMainContainer}
    containerViewStyle={fullWidth ? styles.buttonContainer : {}}
    onPress={() => {
      onClick();
    }}
  />
);

InvertedButton.defaultProps = {
  fullWidth: true,
};

InvertedButton.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  fullWidth: PropTypes.bool,
};

const styles = StyleSheet.create({
  buttonMainContainer: {
    backgroundColor: COLOR_PRIMARY,
    paddingVertical: SPACING_SMALL,
    margin: SPACING_SMALL,
    borderColor: COLOR_PRIMARY,
    borderWidth: 1.5,
  },
  buttonTextContainer: {
    color: COLOR_BACKGROUND,
    fontSize: FONT_SIZE_XSMALL,
  },
  buttonContainer: {
    width: '100%',
  },
});

export default InvertedButton;
