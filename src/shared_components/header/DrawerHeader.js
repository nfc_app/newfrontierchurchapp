import React from 'react';
import PropTypes from 'prop-types';
import { DrawerActions } from 'react-navigation';

import Header from './Header';

const DrawerHeader = props => (
  <Header
    onLeftPress={() => {
      if (props.navigation.state.isDrawerOpen === true) {
        props.navigation.dispatch(DrawerActions.closeDrawer());
      } else {
        props.navigation.dispatch(DrawerActions.openDrawer());
      }
    }}
    onLogoPress={() => {
      props.navigation.navigate('main');
    }}
    leftImage={IMGAGE_HAMBURGER}
  />
);

const IMGAGE_HAMBURGER = require('@assets/images/icon_hamburger_menu.png');

DrawerHeader.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default DrawerHeader;
