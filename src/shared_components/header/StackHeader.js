import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './Header';

const StackHeader = props => (
  <Header
    onLeftPress={() => props.navigation.goBack()}
    onLogoPress={() => {
      props.navigation.navigate(props.currentUser ? 'Main' : 'Login');
    }}
    leftImage={IMAGE_BACK}
  />
);

const IMAGE_BACK = require('@assets/images/icon_back_button.png');

StackHeader.defaultProps = {
  currentUser: null,
};

StackHeader.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUser: PropTypes.string,
};

const mapStateToProps = state => ({
  currentUser: state.currentUser.currentUser,
});

export default connect(mapStateToProps)(StackHeader);
