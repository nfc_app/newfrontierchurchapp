import React from 'react';
import { Image, Platform, StyleSheet, TouchableOpacity, View, YellowBox } from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import { COLOR_PRIMARY, COLOR_PRIMARY_DARK } from '@styles/Common';
import { getStatusBarHeight } from 'react-native-status-bar-height';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const Header = ({ leftImage, onLeftPress, onLogoPress }) => (
  <LinearGradient
    colors={[COLOR_PRIMARY_DARK, COLOR_PRIMARY]}
    start={{ x: 0.0, y: 1.0 }}
    end={{ x: 1.0, y: 1.0 }}
    style={styles.container}
  >
    <TouchableOpacity
      style={styles.leftView}
      onPress={() => {
        onLeftPress();
      }}
    >
      <Image source={leftImage} />
    </TouchableOpacity>

    <TouchableOpacity
      onPress={() => {
        onLogoPress();
      }}
    >
      <Image source={require('@assets/images/icon_logo_nav.png')} style={styles.logoContainer} />
    </TouchableOpacity>

    <View style={styles.rightView} />
  </LinearGradient>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: getStatusBarHeight(true) + 50,
    paddingTop: getStatusBarHeight(true),
    paddingBottom: Platform.OS === 'ios' ? 7 : 5,
  },
  leftView: {
    padding: 25,
    paddingLeft: 15,
    marginTop: 7,
  },
  rightView: {
    paddingHorizontal: 20,
  },
  logoContainer: {
    alignSelf: 'center',
  },
});

Header.propTypes = {
  onLeftPress: PropTypes.func.isRequired,
  onLogoPress: PropTypes.func.isRequired,
  leftImage: PropTypes.number.isRequired,
};

export default Header;
