import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SPACING_XSMALL, SPACING_SMALL, SPACING_MEDIUM, FONT_SIZE_XXSMALL, COLOR_BACKGROUND } from '@styles/Common';
import SubtitleText from '@shared_components/text/SubtitleText';
import BodyText from '@shared_components/text/BodyText';

const StaticContent = prop => (
  <View style={[styles.container, prop.style]}>
    <View>
      {prop.title && <SubtitleText text={prop.title} style={styles.title} />}
      <BodyText style={[styles.textContainer, prop.bodyStyle]} text={prop.body} />
    </View>
    {prop.disclaimer && <Text style={styles.disclaimer}>{prop.disclaimer}</Text>}
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: COLOR_BACKGROUND,
    marginHorizontal: SPACING_SMALL,
  },
  title: {
    paddingVertical: SPACING_XSMALL,
  },
  textContainer: {
    marginBottom: SPACING_MEDIUM,
  },
  disclaimer: {
    paddingVertical: SPACING_SMALL,
    fontSize: FONT_SIZE_XXSMALL,
    fontStyle: 'italic',
  }
});

export default StaticContent;
