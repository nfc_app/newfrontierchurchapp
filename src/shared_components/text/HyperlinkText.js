import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, Linking } from 'react-native';
import { COLOR_HYPERLINK, FONT_SIZE_XSMALL, TEXT_LINE_HEIGHT } from '@styles/Common';

function openLink(url) {
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      console.log(`Don't know how to open ${url}`);
    }
  });
}

const HyperlinkText = prop => <Text style={[styles.body, prop.style]} onPress={() => openLink(prop.url)}>{prop.text}</Text>;

const styles = StyleSheet.create({
  body: {
    fontFamily: 'NanumSquareOTFR',
    fontSize: FONT_SIZE_XSMALL,
    color: COLOR_HYPERLINK,
    lineHeight: TEXT_LINE_HEIGHT,
  },
});

HyperlinkText.propTypes = {
  url: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  style: PropTypes.objectOf(PropTypes.any),
};

export default HyperlinkText;
