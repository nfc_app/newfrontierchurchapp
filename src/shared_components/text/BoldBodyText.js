import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { COLOR_BLACK, FONT_SIZE_XSMALL, TEXT_LINE_HEIGHT } from '@styles/Common';

const BoldBodyText = prop => <Text style={[styles.body, prop.style]}>{prop.text}</Text>;

const styles = StyleSheet.create({
  body: {
    fontFamily: 'NanumSquareOTFR',
    fontWeight: 'bold',
    fontSize: FONT_SIZE_XSMALL,
    color: COLOR_BLACK,
    lineHeight: TEXT_LINE_HEIGHT,
  },
});

export default BoldBodyText;
