import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { COLOR_PRIMARY, FONT_SIZE_MEDIUM, SPACING_XSMALL, TEXT_LINE_HEIGHT } from '@styles/Common';

const SubtitleText = prop => <Text style={[styles.subtitle, prop.style]}>{prop.text}</Text>;

const styles = StyleSheet.create({
  subtitle: {
    fontWeight: 'bold',
    fontFamily: 'NanumSquareOTFEB',
    fontSize: FONT_SIZE_MEDIUM,
    color: COLOR_PRIMARY,
    lineHeight: TEXT_LINE_HEIGHT,
  },
});

export default SubtitleText;
