import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { FONT_SIZE_XLARGE, COLOR_PRIMARY, SPACING_MEDIUM, SPACING_SMALL } from '@styles/Common';

const TitleText = prop => <Text style={[styles.title, prop.style]}>{prop.text}</Text>;

const styles = StyleSheet.create({
  title: {
    fontSize: FONT_SIZE_XLARGE,
    fontWeight: 'bold',
    color: COLOR_PRIMARY,
    marginVertical: SPACING_MEDIUM,
  },
});

export default TitleText;
