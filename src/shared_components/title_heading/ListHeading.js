import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  COLOR_GREY_LIGHT,
  COLOR_GREY_LIGHTEST,
  FONT_SIZE_SMALL,
  SPACING_MEDIUM,
  SPACING_XSMALL,
  COLOR_PRIMARY,
  DIVIDER_HEIGHT,
} from '@styles/Common';

const ListHeading = prop => (
  <View style={styles.viewContainer}>
    <Text style={[styles.textContainer, prop.style]}>{prop.text}</Text>
  </View>
);

const styles = StyleSheet.create({
  viewContainer: {
    width: '100%',
    backgroundColor: COLOR_GREY_LIGHTEST,
    paddingVertical: SPACING_XSMALL,
    paddingHorizontal: SPACING_MEDIUM,
    borderBottomColor: COLOR_GREY_LIGHT,
    borderBottomWidth: DIVIDER_HEIGHT,
  },
  textContainer: {
    fontSize: FONT_SIZE_SMALL,
    color: COLOR_PRIMARY,
  },
});

export default ListHeading;
