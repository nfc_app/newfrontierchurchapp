import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { COLOR_PRIMARY, SPACING_SMALL, FONT_SIZE_XSMALL } from '@styles/Common';

const SubHeading = prop => <Text style={styles.subHeading}>{prop.text}</Text>;

const styles = StyleSheet.create({
  subHeading: {
    fontSize: FONT_SIZE_XSMALL,
    color: COLOR_PRIMARY,
    marginHorizontal: SPACING_SMALL,
  },
});

export default SubHeading;
