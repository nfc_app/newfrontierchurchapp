import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { FONT_SIZE_XLARGE, COLOR_PRIMARY, SPACING_MEDIUM, SPACING_SMALL } from '@styles/Common';

const TitleHeading = prop => <Text style={styles.titleHeading}>{prop.text}</Text>;

const styles = StyleSheet.create({
  titleHeading: {
    fontSize: FONT_SIZE_XLARGE,
    fontWeight: 'bold',
    color: COLOR_PRIMARY,
    marginTop: SPACING_MEDIUM,
    marginBottom: SPACING_SMALL,
    marginHorizontal: SPACING_SMALL,
  },
});

export default TitleHeading;
