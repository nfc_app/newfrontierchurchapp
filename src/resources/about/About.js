// Below about text can currently easily be defined as static "UI Text", but
// it's also possible that this can later be fed via server (ie. more dynamic) in the future.

export const vision = {
  en: [
    {
      type: 'title',
      text: 'NFC IDENTITY',
    },
    {
      type: 'body',
      text:
        'New Frontier Church is a Presbyterian Church centered on the Korean Diaspora, which helps to raise the next generation based Reformative Evangelical theology and to make them live as disciples of Jesus Christ.',
    },
    {
      type: 'title',
      text: 'NFC VISION',
    },
    {
      type: 'body',
      text: 'The vision of New Frontier Church is to nurture and mission to the next generation to live as the salt and light of the world in the gospel of Jesus Christ, and to build the next generations of worship-communities and churches where they are needed.',
    },
    {
      type: 'title',
      text: 'NFC SLOGAN',
    },
    {
      type: 'body',
      text: 'Church for the Next Generation',
    },
    {
      type: 'title',
      text: '3 NFC OBJECTIVES',
    },
    {
      type: 'body',
      text: '1. Education: A Church dedicated to educate the Next Generation',
    },
    {
      type: 'body',
      text: '2. Multi-sites & Church Planting: A church dedicated to building worship communities and churches for the next generation',
    },
    {
      type: 'body',
      text:
        '3. Missional Life: Missional Life: A church that challenges saints to live a life as missionaries in New York and around the world',
    },
  ],
  ko: [
    {
      type: 'title',
      text: 'NFC 정체성',
    },
    {
      type: 'body',
      text:
        '뉴프론티어교회는 개혁주의적 복음주의 신학을 기초로 다음세대를 양육하여 그리스도의 제자로 살아가도록 돕는 한인 디아스포라 중심의 장로교회입니다.',
    },
    {
      type: 'title',
      text: 'NFC VISION',
    },
    {
      type: 'body',
      text: '뉴프론티어교회 비전은 다음 세대들이 그리스도의 복음 안에서 세상의 빛과 소금으로 살아가도록 양육과 선교에 힘쓰고 다음세대 중심의 예배공동체와 교회들을 필요한 곳에 세우는 것입니다.',
    },
    {
      type: 'title',
      text: 'NFC SLOGAN',
    },
    {
      type: 'body',
      text: '다음세대를 세우는 교회 (Church for the Next Generation)',
    },
    {
      type: 'title',
      text: 'NFC 3대 미션',
    },
    {
      type: 'body',
      text: '1. Education: 다음세대를 위한 양육에 힘쓰는 교회',
    },
    {
      type: 'body',
      text: '2. Multi-sites & Church planting: 다음 세대를 위한 예배공동체 및 교회를 세우는 일에 힘쓰는 교회',
    },
    {
      type: 'body',
      text: '3. Missional Life: 성도들이 뉴욕과 전세계로 흩어져 선교적인 삶을 살도록 도전하는 교회',
    },
  ],
};

export const history = {
  en: [
    {
      date: '2006.02. ',
      body:
        'Started as Manhattan Young Adult Ministry of NJ Chodae Church at King’s College located at Empire State Building',
    },
    {
      date: '2007.07. ',
      body:
        'Pastor Song Yongwon resigned, Pastor Inhyun Ryu appointed as Pastor for Young Adult Ministry of NJ Chodae Church',
    },
    {
      date: '2008.02. ',
      body: 'Church relocated to a new site, PS11 William T. Harris Elementary School',
    },
    { date: '2008.10. ', body: 'CCM 11, College Ministry began' },
    {
      date: '2009.06. ',
      body: '"New Frontier Church" established and registered as a religious corporation in NYC',
    },
    {
      date: '2009.09. ',
      body:
        'Missionary center called Vision Center opened at 29 W 30th St. New York, NY. Began our Wednesday Evening and Early Morning services',
    },
    {
      date: '2010.02. ',
      body: 'Began Mustard Seed Offering to help build the Kingdom Professional Center in NYC',
    },
    { date: '2010.07. ', body: 'Cross the Way, prayer walking, held in Manhattan' },
    {
      date: '2010.10. ',
      body:
        'C4K (Couples For Kingdom) Ministry began. Established Chun-Koom-Ah (translates to Kids dreaming of Heaven) as our Sunday School program',
    },
    {
      date: '2011.05. ',
      body:
        'Concerted with NJ Chodae Church, NFC commissioned a missionary, Sooyeon Chung, to China',
    },
    { date: '2011.11. ', body: 'Hosted BaM (Business as Mission) conference' },
    {
      date: '2012.02. ',
      body:
        'The last Sunday worship service held at PS11 due to NYC’s ban on religious services in public schools',
    },
    {
      date: '2012.02. ',
      body: 'Relocated to a new site, Sutton Place (a jewish synagogue) on 51st St.',
    },
    {
      date: '2012.03. ',
      body: 'Relocated back to PS11 after NYC changed rules to allow churches in public schools',
    },
    { date: '2012.09. ', body: 'Vision Center relocated to a new site, 142 W 29th St, New York' },
    {
      date: '2012.09. ',
      body: 'IM Plus to start its Sunday Evening Services on last Sunday of the Month',
    },
    { date: '2012.10. ', body: 'Shodai Japanese Church started its ministry at Vision Center' },
    { date: '2013.10. ', body: 'Hope for Africa' },
    { date: '2013.10. ', body: 'IM Plus began its Sunday Morning services' },
    {
      date: '2013.10. ',
      body: 'Young Adult ministry divided into three fellowships (Kingdom 1, 2, & 3)',
    },
    { date: '2014.04. ', body: 'Joined the presbytery KAPC (Korean American Prebyterian Church)' },
    { date: '2015.02. ', body: 'Bible Academy lectures began' },
    {
      date: '2015.04. ',
      body: 'Hosted Open Word Conference in celebration of the church’s 5th year anniversary',
    },
    {
      date: '2015.05. ',
      body: 'Installed three elders: Hyuksoon Song, Junghang Lee, Youngwon Park',
    },
    {
      date: '2015.12. ',
      body: 'Hosted Compassion Sunday and adapted Uganda tribes as our beneficiary',
    },
    {
      date: '2016.01. ',
      body:
        'Expanded our Sunday worship service to another location, Columbus Campus (PS191) along with Chelsea Campus (PS11)',
    },
    { date: '2017.06. ', body: 'Columbus Campus and Chelsea Campus reunited at PS11 site' },
    { date: '2019.01. ', body: 'Announced the new vision, "Church for the Next Generation"' },
  ],
  ko: [
    {
      date: '2006.02. ',
      body:
        'Empire State Building에 있는 King’s College 강당에서 뉴저지초대교회 맨하탄젊은이예배 시작',
    },
    {
      date: '2007.07. ',
      body: '송용원 목사 사임, 류인현 목사 뉴저지초대교회 젊은이 담당목사로 부임',
    },
    { date: '2008.02. ', body: 'PS11 William T. Harris Elementary School로 예배당 이전' },
    { date: '2008.10. ', body: 'CCM11 대학부 주일예배 시작' },
    { date: '2009.06. ', body: '교회명을 New Frontier Church로 하여 뉴욕시에 종교법인으로 등록' },
    {
      date: '2009.09. ',
      body: '선교관 마련 (29 W 30th St., #202, New York, NY 10001), 수요예배와 새벽기도 시작',
    },
    { date: '2010.02. ', body: '건축을 위한 겨자씨 헌금 시작' },
    { date: '2010.07. ', body: 'Cross The Way 전교인 맨하탄 땅밟기 기도' },
    {
      date: '2010.10. ',
      body:
        'C4K (Couples For Kingdom) 부부공동체 주일예배시작, 주일학교 이름을 천꿈아 (천국을 꿈꾸는 아이들)로 제정',
    },
    { date: '2011.05. ', body: '정수연 선교사 중국 연변 단기선교사로 뉴저지초대교회와 공동 파송' },
    { date: '2011.11. ', body: 'BaM (Business as Mission) 컨퍼런스' },
    { date: '2012.02. ', body: '공립학교 예배 금지 법안에 따라 마지막 예배를 PS11에서 드림' },
    {
      date: '2012.02. ',
      body: '유대회당 (Sutton Place, 51st St)에서 3주동안 (2/19, 2/26, 3/4) 주일예배 드림',
    },
    {
      date: '2012.03. ',
      body: '공립학교 예배 허용법안 판결로 PS11로 복귀, 다시 주일예배장소로 사용',
    },
    { date: '2012.09. ', body: '비전센터 임대 이전 (142 W 29th St, New York)' },
    { date: '2012.09. ', body: 'IM Plus 매월 마지막 주일저녁 영어예배 시작' },
    {
      date: '2012.10. ',
      body: 'Shodai Japanese Church와 협력하여 비전센터에서 일본어 예배 (JM) 시작',
    },
    { date: '2013.06. ', body: '제1회 Hope for Africa' },
    { date: '2013.10. ', body: 'IM Plus 주일오전 영어예배 시작' },
    { date: '2013.10. ', body: '청년부가 킹덤1,2,3 각 3개의 공동체로 나뉨' },
    { date: '2014.04. ', body: 'KAPC 필라델피아노회 가입' },
    { date: '2015.02. ', body: '바이블 아카데미 개강' },
    { date: '2015.04. ', body: '창립5주년 "열린말씀" 컨퍼런스' },
    { date: '2015.05. ', body: '장로장립식 (이정행, 송혁순, 박영원)' },
    { date: '2015.12. ', body: '컴패션 선데이 행사 (우간다 종족 입양)' },
    {
      date: '2016.01. ',
      body: '콜럼버스캠퍼스 (PS191)와 첼시캠퍼스 (PS11)으로 두 곳에서 주일예배시작',
    },
    { date: '2017.06. ', body: '콜럼버스공동체와 첼시공동체가 PS11에서 다시 연합함' },
    { date: '2019.01. ', body: '"다음세대를 세우는 교회", 새 교회 비전이 세워짐' },
  ],
};

export const pastors = {
  en: [
    {
      type: 'row',
      image: require('@assets/images/pastor_ryu.jpg'),
      name: 'Rev. Inhyun Ryu',
      description: [
        {
          type: 'body',
          text: 'Senior Pastor',
        },
        {
          type: 'link',
          text: '267-241-8638',
          url: 'tel:267-241-8638',
        },
        {
          type: 'link',
          text: 'inhyun.ryu@nfcnyc.org',
          url: 'mailto:inhyun.ryu@nfcnyc.org',
        },
        {
          type: 'link',
          text: 'facebook.com/hyun191',
          url: 'https://www.facebook.com/hyun191',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_yoo.jpg'),
      name: 'Rev. Taeyoung Yoo',
      description: [
        {
          type: 'body',
          text: 'Associate Pastor for Married Couples Ministry',
        },
        {
          type: 'link',
          text: '312-343-2499',
          url: 'tel:312-343-2499',
        },
        {
          type: 'link',
          text: 'taeyoung.yoo@nfcnyc.org',
          url: 'mailto:taeyoung.yoo@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_oh.jpg'),
      name: 'Rev. Youngsoo Oh',
      description: [
        {
          type: 'body',
          text: 'Associate Pastor for Young Adults Ministry',
        },
        {
          type: 'link',
          text: '626-316-3484',
          url: 'tel:626-316-3484',
        },
        {
          type: 'link',
          text: 'youngsoo.oh@nfcnyc.org',
          url: 'mailto:youngsoo.oh@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_hwang.jpg'),
      name: 'Jinoh Hwang',
      description: [
        {
          type: 'body',
          text: 'Associate Pastor for Youth & College Ministry, Administration',
        },
        {
          type: 'link',
          text: '718-340-9409',
          url: 'tel:718-340-9409',
        },
        {
          type: 'link',
          text: 'jinoh.hwang@nfcnyc.org',
          url: 'mailto:jinoh.hwang@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_lee.jpg'),
      name: 'Unmi Lee',
      description: [
        {
          type: 'body',
          text: 'Teaching Pastor for Kingdom Toddler 2',
        },
        {
          type: 'link',
          text: '347-301-7091',
          url: 'tel:347-301-7091',
        },
        {
          type: 'link',
          text: 'unmi.lee@nfcnyc.org',
          url: 'mailto:unmi.lee@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_chung.jpg'),
      name: 'Sooyeon Chung',
      description: [
        {
          type: 'body',
          text: 'Education Pastor for Sunday School',
        },
        {
          type: 'link',
          text: '201-851-3260',
          url: 'tel:201-851-3260',
        },
        {
          type: 'link',
          text: 'sooyeon.chung@nfcnyc.org',
          url: 'mailto:sooyeon.chung@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_won.jpg'),
      name: 'Dong Cheol Won',
      description: [
        {
          type: 'body',
          text: 'Education Pastor for Music & Cultural Ministry',
        },
        {
          type: 'link',
          text: '201-310-4966',
          url: 'tel:201-310-4966',
        },
        {
          type: 'link',
          text: 'dongcheol.won@nfcnyc.org',
          url: 'mailto:dongcheol.won@nfcnyc.org',
        },
      ]
    },
  ],
  ko: [
    {
      type: 'row',
      image: require('@assets/images/pastor_ryu.jpg'),
      name: '류인현 담임목사',
      description: [
        {
          type: 'link',
          text: '267-241-8638',
          url: 'tel:267-241-8638',
        },
        {
          type: 'link',
          text: 'inhyun.ryu@nfcnyc.org',
          url: 'mailto:inhyun.ryu@nfcnyc.org',
        },
        {
          type: 'link',
          text: 'facebook.com/hyun191',
          url: 'https://www.facebook.com/hyun191',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_yoo.jpg'),
      name: '유태영 부목사',
      description: [
        {
          type: 'body',
          text: '가정공동체',
        },
        {
          type: 'link',
          text: '312-343-2499',
          url: 'tel:312-343-2499',
        },
        {
          type: 'link',
          text: 'taeyoung.yoo@nfcnyc.org',
          url: 'mailto:taeyoung.yoo@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_oh.jpg'),
      name: '오영수 부목사',
      description: [
        {
          type: 'body',
          text: '청년공동체',
        },
        {
          type: 'link',
          text: '626-316-3484',
          url: 'tel:626-316-3484',
        },
        {
          type: 'link',
          text: 'youngsoo.oh@nfcnyc.org',
          url: 'mailto:youngsoo.oh@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_hwang.jpg'),
      name: '황진오 부목사',
      description: [
        {
          type: 'body',
          text: '대학부, 중고등부, 행정',
        },
        {
          type: 'link',
          text: '718-340-9409',
          url: 'tel:718-340-9409',
        },
        {
          type: 'link',
          text: 'jinoh.hwang@nfcnyc.org',
          url: 'mailto:jinoh.hwang@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_lee.jpg'),
      name: '이은미 전도사',
      description: [
        {
          type: 'body',
          text: '영아2부',
        },
        {
          type: 'link',
          text: '347-301-7091',
          url: 'tel:347-301-7091',
        },
        {
          type: 'link',
          text: 'unmi.lee@nfcnyc.org',
          url: 'mailto:unmi.lee@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_chung.jpg'),
      name: '정수연 교육전도사',
      description: [
        {
          type: 'body',
          text: '유아, 유치부 & 초등부',
        },
        {
          type: 'link',
          text: '201-851-3260',
          url: 'tel:201-851-3260',
        },
        {
          type: 'link',
          text: 'sooyeon.chung@nfcnyc.org',
          url: 'mailto:sooyeon.chung@nfcnyc.org',
        },
      ]
    },
    {
      type: 'row',
      image: require('@assets/images/pastor_won.jpg'),
      name: '원동철 교육전도사',
      description: [
        {
          type: 'body',
          text: '찬양문화사역',
        },
        {
          type: 'link',
          text: '201-310-4966',
          url: 'tel:201-310-4966',
        },
        {
          type: 'link',
          text: 'dongcheol.won@nfcnyc.org',
          url: 'mailto:dongcheol.won@nfcnyc.org',
        },
      ]
    },
  ],
};

export const leadership = {
  en: [
    {
      bold: '\nSenior Pastor: ',
    },
    {
      highlight: 'Rev. Inhyun Ryu ',
      email: 'inhyun.ryu@nfcnyc.org',
      url: 'mailto:inhyun.ryu@nfcnyc.org ',
    },
    {
      bold: '\nSession: ',
    },
    {
      highlight: 'Elder Junghang Lee ',
      email: 'junghang.lee@nfcnyc.org',
      url: 'mailto:junghang.lee@nfcnyc.org',
    },
    {
      highlight: 'Elder Hyuksoon Song ',
      email: 'hyuksoon.song@nfcnyc.org',
      url: 'mailto:hyuksoon.song@nfcnyc.org',
    },
    {
      highlight: 'Elder Youngwon Park ',
      email: 'youngwon.park@nfcnyc.org',
      url: 'mailto:youngwon.park@nfcnyc.org',
    },
    {
      bold: '\nAssociate Pastor: ',
    },
    {
      highlight: 'Rev. Tae Young Yoo ',
      description: '(Married Couples Ministry) ',
      email: 'taeyoung.yoo@nfcnyc.org',
      url: 'mailto:taeyoung.yoo@nfcnyc.org',
    },
    {
      highlight: 'Rev. Youngsoo Oh ',
      description: '(Young Adults Ministry) ',
      email: 'youngsoo.oh@nfcnyc.org',
      url: 'mailto:youngsoo.oh@nfcnyc.org',
    },
    {
      highlight: 'Rev. Jinoh Hwang ',
      description: '(Youth & College Ministry, Administration) ',
      email: 'jinoh.hwang@nfcnyc.org',
      url: 'mailto:jinoh.hwang@nfcnyc.org',
    },
    {
      bold: '\nTeaching Pastor: ',
    },
    {
      highlight: 'Unmi Lee ',
      description: '(Kingdom Toddler 2) ',
      email: 'unmi.lee@nfcnyc.org',
      url: 'mailto:unmi.lee@nfcnyc.org',
    },
    {
      bold: '\nEducation Pastor: ',
    },
    {
      highlight: 'Sooyeon Chung ',
      description: '(Sunday School) ',
      email: 'sooyeon.chung@nfcnyc.org',
      url: 'mailto: sooyeon.chung@nfcnyc.org',
    },
    {
      highlight: 'Dong Cheol Won ',
      description: '(Music & Cultural Ministry) ',
      email: 'dongcheol.won@nfcnyc.org',
      url: 'mailto: dongcheol.won@nfcnyc.org',
    },
    {
      bold: '\nStaff: ',
    },
    {
      highlight: 'Soyeon Park ',
      description: '(Education Director for Kingdom Toddler 1) ',
      email: 'soyeon.park@nfcnyc.org',
      url: 'soyeon.park@nfcnyc.org',
    },
    {
      highlight: 'Heui Ju Kim ',
      description: '(Curriculum Director for Korea School) ',
      email: 'heuijukim@gmail.com',
      url: 'heuijukim@gmail.com',
    },
    {
      highlight: 'Kyuwon Lee ',
      description: '(Video & Media) ',
      email: 'lkw6107@gmail.com',
      url: 'lkw6107@gmail.com',
    },
    {
      bold: '\nDeacon: ',
    },
    {
      highlight: 'Michael Leigh ',
      description: '(Kingdom Toddler 1) ',
      email: 'michael.leigh@nfcnyc.org',
      url: 'mailto:michael.leigh@nfcnyc.org',
    },
    {
      highlight: 'Steven Hong ',
      description: '(Kingdom Toddler 2) ',
      email: 'steven.hong@nfcnyc.org',
      url: 'mailto:steven.hong@nfcnyc.org',
    },
    {
      highlight: 'David Park ',
      description: '(Kingdom Kids) ',
      email: 'david.park@nfcnyc.org',
      url: 'mailto:david.park@nfcnyc.org',
    },
    {
      highlight: 'Ethan Yoon ',
      description: '(Kingdom Seekers) ',
      email: 'ethan.yoon@nfcnyc.org',
      url: 'mailto:ethan.yoon@nfcnyc.org',
    },
    {
      highlight: 'James Park ',
      description: '(Kingdom Makers) ',
      email: 'james.park@nfcnyc.org',
      url: 'mailto:james.park@nfcnyc.org',
    },
    {
      highlight: 'Jiwook Lee ',
      description: '(College Ministry) ',
      email: 'jiwook.lee@nfcnyc.org',
      url: 'mailto:jiwook.lee@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: 'Sean Lee ',
      description: '(Married Couples Ministry) ',
      email: 'sean.lee@nfcnyc.org',
      url: 'mailto:sean.lee@nfcnyc.org',
    },
    {
      highlight: 'Sungwoo Lim ',
      description: '(Married Couples Ministry) ',
      email: 'lim.sungwoo@nfcnyc.org',
      url: 'mailto:lim.sungwoo@nfcnyc.org',
    },
    {
      highlight: 'Kwangmin Choi ',
      description: '(Married Couples Ministry) ',
      email: 'kwangmin.choi@nfcnyc.org',
      url: 'mailto:kwangmin.choi@nfcnyc.org',
    },
    {
      highlight: 'Dan Ha ',
      description: '(Married Couples Ministry) ',
      email: 'dan.ha@nfcnyc.org',
      url: 'mailto:dan.ha@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: 'Minwook Park ',
      description: '(Finance) ',
      email: 'minwook.park@nfcnyc.org',
      url: 'mailto:minwook.park@nfcnyc.org',
    },
    {
      highlight: 'Doosik Lew ',
      description: '(Finance) ',
      email: 'doosik.lew@nfcnyc.org',
      url: 'mailto:doosik.lew@nfcnyc.org',
    },
    {
      highlight: 'Joonil Cho ',
      description: '(Finance) ',
      email: 'joonil.cho@nfcnyc.org',
      url: 'mailto:joonil.cho@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: 'Dohyung Kim ',
      description: '(New Comers) ',
      email: 'dohyung.kim@nfcnyc.org',
      url: 'mailto:dohyung.kim@nfcnyc.org',
    },
    {
      highlight: 'Taeyoung Kim ',
      description: '(Serving) ',
      email: 'taeyoung.kim@nfcnyc.org',
      url: 'mailto:taeyoung.kim@nfcnyc.org',
    },
    {
      highlight: 'Ian Kim ',
      description: '(Worship) ',
      email: 'ian.kim@nfcnyc.org',
      url: 'mailto:ian.kim@nfcnyc.org',
    },
    {
      highlight: 'Shin Lee ',
      description: '(Compassion) ',
      email: 'shin.lee@nfcnyc.org',
      url: 'mailto:shin.lee@nfcnyc.org',
    },
    {
      highlight: 'Brian Lee ',
      description: '(Mission) ',
      email: 'brian.lee@nfcnyc.org',
      url: 'mailto:brian.lee@nfcnyc.org',
    },
    {
      highlight: 'Jonghwa Lee ',
      description: '(Intercessory Prayer) ',
      email: 'jonghwa.lee@nfcnyc.org',
      url: 'mailto:jonghwa.lee@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: 'Jon Im ',
      description: '(Communication) ',
      email: 'jon.im@nfcnyc.org',
      url: 'mailto:jon.im@nfcnyc.org',
    },
    {
      highlight: 'Yongpil Choi ',
      description: '(Cultural Ministry) ',
      email: 'yongpil.choi@nfcnyc.org',
      url: 'mailto:yongpil.choi@nfcnyc.org',
    },
    {
      highlight: 'Joonyoung Im ',
      description: '(Praise) ',
      email: 'joonyoung.im@nfcnyc.org',
      url: 'mailto:joonyoung.im@nfcnyc.org',
    },
    {
      highlight: 'Paul Cho ',
      description: '(Choir) ',
      email: 'paul.cho@nfcnyc.org',
      url: 'mailto:paul.cho@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: 'Howard Chun ',
      description: '(Future Planing NY, Interpretation) ',
      email: 'howard.chun@nfcnyc.org',
      url: 'mailto:howard.chun@nfcnyc.org',
    },
    {
      highlight: 'Daniel Joo ',
      description: '(Future Planing NJ, Interpretation) ',
      email: 'daniel.joo@nfcnyc.org',
      url: 'mailto:daniel.joo@nfcnyc.org',
    },
  ],
  ko: [
    {
      bold: '\n담임목사: ',
    },
    {
      highlight: '류인현 목사 ',
      email: 'inhyun.ryu@nfcnyc.org',
      url: 'mailto:inhyun.ryu@nfcnyc.org',
    },
    {
      bold: '\n당회: ',
    },
    {
      highlight: '이정행 장로 ',
      email: 'junghang.lee@nfcnyc.org',
      url: 'mailto:junghang.lee@nfcnyc.org',
    },
    {
      highlight: '송혁순 장로 ',
      email: 'hyuksoon.song@nfcnyc.org',
      url: 'mailto:hyuksoon.song@nfcnyc.org',
    },
    {
      highlight: '박영원 장로 ',
      email: 'youngwon.park@nfcnyc.org',
      url: 'mailto:youngwon.park@nfcnyc.org',
    },
    {
      bold: '\n부목사: ',
    },
    {
      highlight: '유태영 목사 ',
      description: '(가정공동체) ',
      email: 'taeyoung.yoo@nfcnyc.org',
      url: 'mailto:taeyoung.yoo@nfcnyc.org',
    },
    {
      highlight: '오영수 목사 ',
      description: '(청년공동체) ',
      email: 'youngsoo.oh@nfcnyc.org',
      url: 'mailto:youngsoo.oh@nfcnyc.org',
    },
    {
      highlight: '황진오 목사 ',
      description: '(중고등부, 대학부, 행정) ',
      email: 'jinoh.hwang@nfcnyc.org',
      url: 'mailto:jinoh.hwang@nfcnyc.org',
    },
    {
      bold: '\n전도사: ',
    },
    {
      highlight: '이은미 전도사 ',
      description: '(영아2부) ',
      email: 'unmi.lee@nfcnyc.org',
      url: 'mailto:unmi.lee@nfcnyc.org',
    },
    {
      bold: '\n교육전도사: ',
    },
    {
      highlight: '정수연 교육전도사 ',
      description: '(유아부, 유치부, 초등부) ',
      email: 'sooyeon.chung@nfcnyc.org',
      url: 'mailto:sooyeon.chung@nfcnyc.org',
    },
    {
      highlight: '원동철 교육전도사 ',
      description: '(찬양문화사역) ',
      email: 'dongcheol.won@nfcnyc.org',
      url: 'mailto:dongcheol.won@nfcnyc.org',
    },
    {
      bold: '\n간사: ',
    },
    {
      highlight: '박소연 교육간사 ',
      description: '(영아1부) ',
      email: 'soyeon.park@nfcnyc.org',
      url: 'mailto:soyeon.park@nfcnyc.org',
    },
    {
      highlight: '김희주 간사 ',
      description: '(한국학교) ',
      email: 'heuijukim@gmail.com',
      url: 'mailto:heuijukim@gmail.com',
    },
    {
      highlight: '이규원 간사 ',
      description: '(미디어) ',
      email: 'lkw6107@gmail.com',
      url: 'mailto:lkw6107@gmail.com',
    },
    {
      bold: '\n집사: ',
    },
    {
      highlight: '이강국 집사 ',
      description: '(영아1부) ',
      email: 'michael.leigh@nfcnyc.org',
      url: 'mailto:michael.leigh@nfcnyc.org',
    },
    {
      highlight: '홍순철 집사 ',
      description: '(영아2부) ',
      email: 'steven.hong@nfcnyc.org',
      url: 'mailto:steven.hong@nfcnyc.org',
    },
    {
      highlight: '박철민 집사 ',
      description: '(유아, 유치부) ',
      email: 'david.park@nfcnyc.org',
      url: 'mailto:david.park@nfcnyc.org',
    },
    {
      highlight: '윤호석 집사 ',
      description: '(초등부) ',
      email: 'ethan.yoon@nfcnyc.org',
      url: 'mailto:ethan.yoon@nfcnyc.org',
    },
    {
      highlight: '박성진 집사 ',
      description: '(중고등부) ',
      email: 'james.park@nfcnyc.org',
      url: 'mailto:james.park@nfcnyc.org',
    },
    {
      highlight: '이지욱 집사 ',
      description: '(대학부) ',
      email: 'jiwook.lee@nfcnyc.org',
      url: 'mailto:jiwook.lee@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: '이승현 집사 ',
      description: '(가정공동체) ',
      email: 'sean.lee@nfcnyc.org',
      url: 'mailto:sean.lee@nfcnyc.org',
    },
    {
      highlight: '임성우 집사 ',
      description: '(가정공동체) ',
      email: 'lim.sungwoo@nfcnyc.org',
      url: 'mailto:lim.sungwoo@nfcnyc.org',
    },
    {
      highlight: '최광민 집사 ',
      description: '(가정공동체) ',
      email: 'kwangmin.choi@nfcnyc.org',
      url: 'mailto:kwangmin.choi@nfcnyc.org',
    },
    {
      highlight: '하영돈 집사 ',
      description: '(가정공동체) ',
      email: 'dan.ha@nfcnyc.org',
      url: 'mailto:dan.ha@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: '박민욱 집사 ',
      description: '(재정) ',
      email: 'minwook.park@nfcnyc.org',
      url: 'mailto:minwook.park@nfcnyc.org',
    },
    {
      highlight: '유두식 집사 ',
      description: '(재정) ',
      email: 'doosik.lew@nfcnyc.org',
      url: 'mailto:doosik.lew@nfcnyc.org',
    },
    {
      highlight: '조준일 집사 ',
      description: '(재정) ',
      email: 'joonil.cho@nfcnyc.org',
      url: 'mailto:joonil.cho@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: '김도형 집사 ',
      description: '(새가족) ',
      email: 'dohyung.kim@nfcnyc.org',
      url: 'mailto:dohyung.kim@nfcnyc.org',
    },
    {
      highlight: '김태영 집사 ',
      description: '(섬김) ',
      email: 'taeyoung.kim@nfcnyc.org',
      url: 'mailto:taeyoung.kim@nfcnyc.org',
    },
    {
      highlight: '김이안 집사 ',
      description: '(예배) ',
      email: 'ian.kim@nfcnyc.org',
      url: 'mailto:ian.kim@nfcnyc.org',
    },
    {
      highlight: '이신욱 집사 ',
      description: '(긍휼사역) ',
      email: 'shin.lee@nfcnyc.org',
      url: 'mailto:shin.lee@nfcnyc.org',
    },
    {
      highlight: '이웅 집사 ',
      description: '(선교) ',
      email: 'brian.lee@nfcnyc.org',
      url: 'mailto:brian.lee@nfcnyc.org',
    },
    {
      highlight: '이종화 집사 ',
      description: '(중보기도) ',
      email: 'jonghwa.lee@nfcnyc.org',
      url: 'mailto:jonghwa.lee@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: '임종현 집사 ',
      description: '(커뮤니케이션) ',
      email: 'jon.im@nfcnyc.org',
      url: 'mailto:jon.im@nfcnyc.org',
    },
    {
      highlight: '최용필 집사 ',
      description: '(문화사역) ',
      email: 'yongpil.choi@nfcnyc.org',
      url: 'mailto:yongpil.choi@nfcnyc.org',
    },
    {
      highlight: '임준영 집사 ',
      description: '(찬양) ',
      email: 'joonyoung.im@nfcnyc.org',
      url: 'mailto:joonyoung.im@nfcnyc.org',
    },
    {
      highlight: '조원진 집사 ',
      description: '(찬양대) ',
      email: 'paul.cho@nfcnyc.org',
      url: 'mailto:paul.cho@nfcnyc.org',
    },
    {
      bold: '',
    },
    {
      highlight: '전찬호 집사 ',
      description: '(미래준비NY, 통역) ',
      email: 'howard.chun@nfcnyc.org',
      url: 'mailto:howard.chun@nfcnyc.org',
    },
    {
      highlight: '주광모 집사 ',
      description: '(미래준비NJ, 통역) ',
      email: 'daniel.joo@nfcnyc.org',
      url: 'mailto:daniel.joo@nfcnyc.org',
    },
  ],
};
