// Below next generation text can currently easily be defined as static "UI Text", but
// it's also possible that this can later be fed via server (ie. more dynamic) in the future.

export const sundaySchool = {
  en: {
    title: 'Sunday School',
    body:
      'New Frontier Church Sunday School is a bible education institution. Students range from toddler to high school students. Our goal is to teach our children the basics of Christian faith. We hope to prepare our children in the faith of Christ Jesus trusting them to be faithful servants in the kingdom of God. We sincerely hope and pray that our unrelenting endeavor will bear fruit, so that our children will stand firm in the faith of Christ our Lord in this world inundated with secular values.',
    disclaimer: 'This activity is not sponsored or endorsed by the New York City Department of Education or the City of New York.',
  },
  ko: {
    title: '주일학교',
    body:
      '뉴프론티어교회 주일학교는 영아에서 중고등부까지의 아이들을 예수그리스도안에서 하나님의 말씀으로 양육하는 교육 기관입니다. 아이들이 믿음 안에서 성장하여 복음과 하나님의 나라를 위하여 귀히 쓰임 받는 아이들이 될 수 있도록 신앙의 기본을 올바로 가르치기를 소망하는 학교입니다. 성경 말씀을 바로 배우고 또 말씀대로 살 수 있도록 잘 준비되어 여러 가지 가치관이 난무하는 세상 속에서 흔들림이 없는 믿음의 자녀로 성장할 수 있도록 가르치고 배우고 성장하는 학교입니다.',
    disclaimer: 'This activity is not sponsored or endorsed by the New York City Department of Education or the City of New York.',
  },
};

export const koreaSchool = {
  en: {
    title: 'Korea School',
    body:
      "New Frontier Academy is an institution with a goal of teaching students Korean language as well as Korean culture and history. Improving our students' Korean language skills and establishing their identities as Korean Americans with firm sense of Korean heritage through cultural and historical lessons are the main mission of New Frontier Academy. In addition, New Frontier Academy and its Christian teachers aim to raise a new generation that will carry New Frontier Church's mission of helping others and doing good for the society in the name of Jesus Christ.",
  },
  ko: {
    title: '한국학교',
    body:
      '뉴프론티어 아카데미는 미국에서 자라나는 아이들에게 한글과 한국의 문화와 역사를 가르치는 한국어 교육 기관입니다. \n\n학생들의 한국어 실력을 향상시키고, 한국의 문화와 역사 교육을 통해 아이들이 한국인으로서의 올바른 정체성과 자긍심을 가진 Korean American으로 자랄 수 있도록 도와주는 것을 목표로 하고 있습니다. \n\n또한, 지역 사회를 섬기고 다음 세대를 길러 내는 일에 투자하고자 하는 교회의 비전을 함께 품고, 기독교 신앙을 가진 교사들이 그리스도의 마음으로 아이들을 가르치는 학교입니다. \n\n만 4세 유아 한글 반부터 SAT II를 준비하는 한국어 고급 반까지 총 8개의 반을 운영하고 있으며, 창의 미술과 컴퓨터 프로그래밍과 같은 특별활동에 한국어를 접목하여 다양한 환경에서 한국어를 익히고 사용할 수 있는 환경을 제공하고 있습니다',
  },
};
