// Initial Implementation of the Global UI Text Definition.
// This should be refactored into separate files if needed.
// Also, there should be an interface to which different languages should implement with values
// but I am currently unfamiliar with how to create interfaces/implementations with pure JS.

const uiTextMap = {
  ko: {
    nav: {
      home: '홈',
      about: 'NFC 소개',
      location: '예배 정보',
      qt: '이번주 QT',
      forms: '신청서',
      sermon: '설교 모음',
      nextgen: '다음 세대',
      prayer: '기도',
      offering: '헌금',
      contact: '연락 정보',
      settings: '설정',
    },
    main: {
      header: '환영합니다!',
    },
    about: {
      header: 'NFC 소개',
      vision: '비전',
      history: '역사',
      pastors: '섬기는분',
      leadership: '조직도',
    },
    qt: {
      header: '오늘의 QT말씀',
      interface: {
        copyVersesButton: '말씀 복사하기',
      },
      copyright: '',
    },
    announcement: {
      header: 'NFC 소식',
    },
    meeting: {
      header: '이번주 모임',
    },
    sermon: {
      header: '설교 모음',
    },
    nextgen: {
      header: '다음 세대',
    },
    forms: {
      header: '신청서',
    },
    location: {
      header: '예배 정보',
      sundayHeader: '주일 예배',
      weekdayHeader: '주중 예배',
      disclaimer:
        'This activity is not sponsored or endorsed by the New York City Department of Education or the City of New York.',
    },
    contact: {
      header: '연락 정보',
      bodyText: [
        {
          body:
            '우리 교회는 성도 여러분의 목소리에 늘 귀 기울이고 있습니다. 교회에 문의 사항, 불편 사항, 제안등 무엇이든지 연락주세요.',
        },
      ],
      interface: {
        emailButton: 'contact@nfcnyc.org 이메일하기',
        url: 'mailto:contact@nfcnyc.org',
      },
    },
    offering: {
      header: '헌금',
      bodyText: [
        {
          type: 'body',
          text: '일반 온라인 헌금을 원하시는 분들은 Zelle (for Domestic)과 Paypal (for International)을 이용하시기 바랍니다.\n\n',
        },
        {
          type: 'title',
          text: 'DOMESTIC (ZELLE)\n',
        },
        {
          type: 'body',
          text: '사용 중이신 은행의 웹페이지나 앱을 통해 직접 보내실 수 있습니다. 보내실 곳 - ',
        },
        {
          type: 'highlight',
          text: 'offering@nfcnyc.org\n',
        },
        {
          type: 'link',
          text: '여기 ',
          url: 'https://www.zellepay.com/get-started/'
        },
        {
          type: 'body',
          text: '링크로 가셔서 본인이 사용 중인 은행의 Zelle 사용법을 확인하실 수 있습니다.\n\n',
        },
        {
          type: 'title',
          text: 'INTERNATIONAL (PAYPAL)\n',
        },
        {
          type: 'body',
          text: 'Paypal의 경우 *수수료를 제외한 금액이 기록됩니다.\n',
        },
        {
          type: 'link',
          text: '여기',
          url: 'https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=nfc.finance.team@gmail.com&lc=US&item_name=Donate&no_note=0&no_shipping=2&curency_code=USD&bn=PP-DonationsBF:btn_donateCC_LG.gif:NonHosted',
        },
        {
          type: 'body',
          text: '를 클릭하셔서 페이팔 페이지를 이용하시기 바랍니다.\n\n',
        },
        {
          type: 'footprint',
          text: '*Paypal 수수료 안내\n헌금액 $1 - Paypal 수수료 $0.32\n헌금액 $100 - Paypal 수수료 $2.50\n헌금액 $5,000 - Paypal 수수료 $110.30\n',
        },
      ],
    },
    prayer: {
      header: '기도',
      button: '기도 요청',
      link: 'https://goo.gl/vCF9Ey',
      bodyText: [
        {
          title: 'NFC 공동체 기도',
          body:
            '1. 예수 그리스도가 주인되시고 하나님이 홀로 영광받으시는 뉴프론티어교회가 되도록\n2. 모든 교인이 예배를 사모하는 마음과 교회를 사랑하는 청지기 의식을 가질 수 있도록\n3. 말씀 양육을 통해 교인들이 영적으로 성장할 수 있도록\n4. 교역자와 말씀 사역자들이 말씀 위에 굳게 서도록\n5. 교회 리더들의 겸손과 교인들의 순종으로 교회가 하나가 되도록\n6. 예수 그리스도의 사랑이 넘치는 교회가 되도록',
        },
        {
          title: '중보기도요청',
          body:
            '교회 중보기도팀에게 기도를 요청하세요. 나누고 싶으신 기도 제목이 있으시면, 아래 버튼을 눌러서 NFC 중보기도팀으로 보내주세요. 모든 기도 제목은 confidential 입니다.',
        },
      ],
    },
    settings: {
      header: '설정',
      menu: {
        language: '언어',
      },
      interface: {
        logOutButton: '로그아웃',
      },
    },
    languageSetting: {
      header: 'Language',
      options: {
        ko: '한국어',
        en: 'English',
      },
    },
  },
  en: {
    nav: {
      home: 'Home',
      about: 'About NFC',
      location: 'Services & Location',
      qt: 'Quiet Time',
      forms: 'Applications',
      sermon: 'Sermon',
      nextgen: 'Next Generation',
      prayer: 'Prayer',
      offering: 'Offering',
      contact: 'Contact',
      settings: 'Settings',
    },
    main: {
      header: 'Welcome!',
    },
    about: {
      header: 'About \nNew Frontier \nChurch',
      vision: 'Vision',
      history: 'History',
      pastors: 'Pastors',
      leadership: 'Leadership',
    },
    qt: {
      header: 'Daily QT',
      interface: {
        copyVersesButton: 'Copy Verses',
      },
      copyright:
        'Scripture quotations are from the ESV® Bible (The Holy Bible, English Standard Version®), copyright © 2001 by Crossway, a publishing ministry of Good News Publishers. Used by permission. All rights reserved. You may not copy or download more than 500 consecutive verses of the ESV Bible or more than one half of any book of the ESV Bible.',
    },
    announcement: {
      header: 'NFC Announcements',
      hardcodedAnnoucements:
        'We currently do not publish our weekly church announcements in English.\n\nTo learn more about our up-to-date news or have any questions about our church, please contact Howard Chun at howard.chun@nfcnyc.org',
    },
    meeting: {
      header: 'Meetings This Week',
    },
    sermon: {
      header: 'Sermon',
    },
    nextgen: {
      header: 'Next Generation',
    },
    forms: {
      header: 'Applications',
    },
    location: {
      header: 'Services & \nLocations',
      sundayHeader: 'Sunday',
      weekdayHeader: 'Weekday',
      disclaimer:
        'This activity is not sponsored or endorsed by the New York City Department of Education or the City of New York.',
    },
    contact: {
      header: 'Contact',
      bodyText: [
        {
          body:
            'The church is always open to your feedback. Please contact us with any questions, complaints, and suggestions.',
        },
      ],
      interface: {
        emailButton: 'Email contact@nfcnyc.org',
        url: 'mailto:contact@nfcnyc.org',
      },
    },
    offering: {
      header: 'Offering',
      bodyText: [
        {
          type: 'body',
          text: 'If you’d like to donate online, you can give through Zelle (for Domestic) or Paypal (for International).\n\n',
        },
        {
          type: 'title',
          text: 'DOMESTIC (ZELLE)\n',
        },
        {
          type: 'body',
          text: 'You can give via Zelle using your bank’s mobile app or online banking. Send donations to - ',
        },
        {
          type: 'highlight',
          text: 'offering@nfcnyc.org\n',
        },
        {
          type: 'body',
          text: 'Click ',
        },
        {
          type: 'link',
          text: 'here ',
          url: 'https://www.zellepay.com/get-started/'
        },
        {
          type: 'body',
          text: 'to get started with Zelle\n\n',
        },
        {
          type: 'title',
          text: 'INTERNATIONAL (PAYPAL)\n',
        },
        {
          type: 'body',
          text: 'When using Paypal, your donation amount minus *a corresponding processing fee will be recorded.\nPlease click ',
        },
        {
          type: 'link',
          text: 'here',
          url: 'https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=nfc.finance.team@gmail.com&lc=US&item_name=Donate&no_note=0&no_shipping=2&curency_code=USD&bn=PP-DonationsBF:btn_donateCC_LG.gif:NonHosted',
        },
        {
          type: 'body',
          text: ' to give a single gift, or schedule recurring giving using our PayPal account.\n\n',
        },
        {
          type: 'footprint',
          text: '*Paypal processing fee\nDonation amount $1 - Paypal fee $0.32\nDonation amount $100 - Paypal fee $2.50\nDonation amount $5,000 - Paypal fee $110.30\n',
        },
      ],
    },
    prayer: {
      header: 'Prayer',
      button: 'Send Prayer Request',
      link: 'https://goo.gl/vCF9Ey',
      bodyText: [
        {
          title: 'NEW FRONTIER CONGREGATION PRAYER',
          body:
            "1. A church Jesus Christ is Lord and God is glorified\n2. A congregation back to the heart of worship and service for the church\n3. Spiritual maturity of our members through the word of God\n4. Pastors to have solid foundation of God's word\n5. Leadership to have humble and obedient hearts\n6. A church full of love of Jesus Christ",
        },
        {
          title: 'NEED A PRAYER?',
          body:
            'You can send your prayer requests to our intercessory prayer team to receive prayers by clicking the button below. All prayer requests are strictly confidential.',
        },
      ],
    },
    settings: {
      header: 'Settings',
      menu: {
        language: 'Language',
      },
      interface: {
        logOutButton: 'Log Out',
      },
    },
    languageSetting: {
      header: 'Language',
      options: {
        ko: '한국어',
        en: 'English',
      },
    },
  },
};

export default uiTextMap;
