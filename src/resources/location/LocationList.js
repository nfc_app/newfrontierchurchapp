// Below location list can currently easily be defined as static "UI Text", but
// it's also possible that this can later be fed via server (ie. more dynamic) in the future.
// Plus, UIText contents should try to be more dispersed as we enhance the app.

export const sundayLocationList = {
  en: [
    {
      title: 'Morning Worship Service (English Interpretation)',
      time: '11:15 AM',
      location: 'PS11 - 320 W 21st St, New York, NY 10011',
      context: 'Family Ministry, IM+',
    },
    {
      title: 'Afternoon Worship Service',
      time: '1:30 PM',
      location: 'PS11 - 320 W 21st St, New York, NY 10011',
      context: 'College Ministry, Young Adult Ministry',
    },
  ],
  ko: [
    {
      title: '1부 예배 (영어통역)',
      time: '오전 11:15',
      location: 'PS11 - 320 W 21st St, New York, NY 10011',
      context: '가정공동체, IM+',
    },
    {
      title: '2부 예배',
      time: '오후 1:30',
      location: 'PS11 - 320 W 21st St, New York, NY 10011',
      context: '대학 공동체, 청년 공동체',
    },
  ],
};

export const weekdayLocationList = {
  en: [
    {
      title: 'Wednesday Worship Service',
      time: 'Wednesday, 7:30 PM',
      location: 'Vision Center - 142 W 29th St, New York, NY 10001',
    },
    {
      title: 'Early Morning Service',
      time: 'Tuesday - Friday, 6:45 AM',
      location: 'Vision Center - 142 W 29th St, New York, NY 10001',
    },
    {
      title: 'Saturday Vision Service',
      time: 'First Saturday of every month, 6:45 AM',
      location: 'Vision Center - 142 W 29th St, New York, NY 10001',
    },
    {
      title: 'New Jersey Early Morning Service',
      time: 'Wednesday & Friday, 6:30 AM',
      location: '400 Commercial Ave, Palisades Park, NJ 07650',
    },
  ],
  ko: [
    {
      title: '수요 브릿지 예배',
      time: '수요일, 오후 7:30',
      location: '비전센터 - 142 W 29th St, New York, NY 10001',
    },
    {
      title: '새벽 예배',
      time: '화요일 - 금요일, 오전 6:45',
      location: '비전센터 - 142 W 29th St, New York, NY 10001',
    },
    {
      title: '토요 비전 예배',
      time: '매달 첫재 토, 6:45 AM',
      location: '비전센터 - 142 W 29th St, New York, NY 10001',
    },
    {
      title: '뉴저지 새벽 예배',
      time: '수 & 금, 6:30 AM',
      location: '400 Commercial Ave, Palisades Park, NJ 07650',
    },
  ],
};
