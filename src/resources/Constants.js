// Place to store global constants, eg. server address.
// PLEASE KEEP GLOBAL CONSTANTS TO A MINIMUM.

export const appConstants = {
  serverAddr: 'http://api.nfcnyc.org:9191/',
  version: 2.0,
  defaultLang: 'ko',
  timeToFetch: 86400000,
};
