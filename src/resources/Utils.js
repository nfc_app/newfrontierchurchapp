export function getNextSunday() {
  let date = new Date();
  date.setDate(date.getDate() + (6 - date.getDay()) % 7 + 1);
  date.setHours(0,0,0,0);
  return date;
}
