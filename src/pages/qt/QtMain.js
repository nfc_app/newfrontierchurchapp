import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ScrollView, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { qtIndexSet, fetchQtsIfNeeded } from '@actions/qt';
import { COLOR_BLACK, COLOR_BACKGROUND } from '@styles/Common';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import ListHeading from '@shared_components/title_heading/ListHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import QtDayList from './components/QtDayList';

class QtMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    this.props.getQt(false);
  }

  onRefresh = () => {
    const { getQt } = this.props;
    this.setState({ refreshing: true });
    getQt(true)
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch(() => {
        alert('Network error - could not fetch QT!');
        this.setState({ refreshing: false });
      });
  };

  render() {
    const { uiText, qtContents, onClick, startingDate, navigation } = this.props;
    const { refreshing } = this.state;
    return (
      <View style={styles.container}>
        <DrawerHeader navigation={navigation} />
        <ScrollView
          style={styles.scrollContainer}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
        >
          <TitleHeading text={uiText.header} />
          <ListHeading text={startingDate} style={{ color: COLOR_BLACK }} />
          <QtDayList onClick={onClick} qtContents={qtContents} navigation={navigation} />
        </ScrollView>
      </View>
    );
  }
}

QtMain.propTypes = {
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  qtContents: PropTypes.arrayOf(
    PropTypes.shape({
      qtMetaData: PropTypes.shape({
        dayOfWeek: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        qtTitle: PropTypes.string.isRequired,
      }),
    }).isRequired
  ).isRequired,
  onClick: PropTypes.func.isRequired,
  startingDate: PropTypes.string.isRequired,
  getQt: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
});

function getQtContents(qt, lang) {
  return qt.qtContent[lang];
}

const getQtInfo = qt => qt.startingDate;

const mapStateToProps = state => ({
  uiText: state.uiText.qt,
  qtContents: getQtContents(state.qt, state.session.lang),
  startingDate: getQtInfo(state.qt),
});

const mapDispatchToProps = dispatch => ({
  onClick: value => {
    console.log(`QT index selected to value ${value}`);
    dispatch(qtIndexSet(value));
  },
  getQt: (force) => dispatch(fetchQtsIfNeeded(force)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QtMain);
