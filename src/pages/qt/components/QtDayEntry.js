import React from 'react';
import PropTypes from 'prop-types';
import ListRow from '@shared_components/row_item/ListRow';

const QtDayEntry = ({ qtOfTheDayInfo, onClick, indexVal }) => (
  <ListRow
    onPress={() => {
      onClick(indexVal);
    }}
    showArrow
    text={`${qtOfTheDayInfo.dayOfWeek} ${qtOfTheDayInfo.date}`}
    subtext={qtOfTheDayInfo.qtTitle}
    key={indexVal}
  />
);

QtDayEntry.propTypes = {
  qtOfTheDayInfo: PropTypes.shape({
    dayOfWeek: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    qtTitle: PropTypes.string.isRequired,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
  indexVal: PropTypes.number.isRequired,
};

export default QtDayEntry;
