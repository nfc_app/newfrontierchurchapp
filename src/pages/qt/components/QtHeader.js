import React from 'react';
import PropTypes from 'prop-types';
import { Image, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import {
  FONT_SIZE_MEDIUM,
  SPACING_MEDIUM,
  COLOR_PRIMARY,
  COLOR_GREY_LIGHT,
  COLOR_GREY_LIGHTEST,
  COLOR_GREY_DARK,
} from '@styles/Common';

const QtHeader = ({ qtOfTheDayInfo, onPrevClick, onNextClick, prev, next }) => (
  <View style={styles.headerContainer}>
    <TouchableOpacity style={styles.verseNavContainer} onPress={onPrevClick} >
      {prev.date && <Text numberOfLines={1} style={styles.verseNavText}>
        {prev.dayOfWeek}
      </Text>}
      {prev.date && <Image source={require('@assets/images/qt_arrow_left.png')} />}
    </TouchableOpacity>

    <Text style={styles.verseHeaderContainer}>
      {qtOfTheDayInfo.dayOfWeek}, {qtOfTheDayInfo.date}
    </Text>

    <TouchableOpacity style={styles.verseNavContainer} onPress={onNextClick} >
      {next.date && <Image source={require('@assets/images/qt_arrow_right.png')} />}
      {next.date && <Text numberOfLines={1} style={styles.verseNavText}>
        {next.dayOfWeek}
      </Text>}
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    backgroundColor: COLOR_GREY_LIGHTEST,
  },
  verseNavContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLOR_GREY_LIGHT,
    width: 100,
  },
  verseNavText: {
    fontSize: FONT_SIZE_MEDIUM,
    color: COLOR_GREY_DARK,
    fontWeight: 'bold',
    fontFamily: 'SourceSansPro-Semibold',
    flex: 1,
  },
  verseHeaderContainer: {
    fontSize: FONT_SIZE_MEDIUM,
    fontWeight: 'bold',
    fontFamily: 'SourceSansPro-Semibold',
    textAlign: 'center',
    color: COLOR_PRIMARY,
    paddingVertical: SPACING_MEDIUM,
    width: 225,
  },
});

QtHeader.propTypes = {
  // qtOfTheDayInfo might need to contain other fields
  qtOfTheDayInfo: PropTypes.shape({
    dayOfWeek: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
  }).isRequired,
  onPrevClick: PropTypes.func.isRequired,
  onNextClick: PropTypes.func.isRequired,
  prev: PropTypes.shape({
    dayOfWeek: PropTypes.string,
    date: PropTypes.string,
  }).isRequired,
  next: PropTypes.shape({
    dayOfWeek: PropTypes.string,
    date: PropTypes.string,
  }).isRequired,
};

export default QtHeader;
