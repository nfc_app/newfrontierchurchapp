import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import QtDayEntry from './QtDayEntry';

const QtDayList = ({ qtContents, onClick, navigation }) => (
  <View>
    {qtContents && qtContents.map((qtContent, index) => (
      <QtDayEntry
        key={index}
        qtOfTheDayInfo={qtContent.qtMetaData}
        onClick={value => {
          onClick(value);
          navigation.navigate('QtContent');
        }}
        indexVal={index}
      />
    ))}
  </View>
);

QtDayList.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  qtContents: PropTypes.arrayOf(
    PropTypes.shape({
      qtMetaData: PropTypes.shape({
        dayOfWeek: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        qtTitle: PropTypes.string.isRequired,
      }),
    }).isRequired
  ).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default QtDayList;
