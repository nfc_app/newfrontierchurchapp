import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, ScrollView, Text, View } from 'react-native';
import Verse from './Verse';

const VerseList = ({ qtInfo, verses }) => (
  <View>
    <Text style={styles.infoContainer}>{qtInfo}</Text>
    {verses.map(verse => (
      <Verse key={verse.verseNum} {...verse} />
    ))}
  </View>
);

VerseList.propTypes = {
  // qtInfo might need to be broken down to book, chapter, verse range.
  qtInfo: PropTypes.string.isRequired,
  verses: PropTypes.arrayOf(
    PropTypes.shape({
      verseNum: PropTypes.string.isRequired,
      verseText: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
};

const styles = StyleSheet.create({
  infoContainer: {
    fontSize: 16,
    color: '#e9128b',
    paddingBottom: 10,
  },
});

export default VerseList;
