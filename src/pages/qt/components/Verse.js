import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text } from 'react-native';

const Verse = ({ verseNum, verseText }) => (
  <Text style={styles.headerContainer}>
    {verseNum}. {verseText.trim()}
  </Text>
);

const styles = StyleSheet.create({
  headerContainer: {
    paddingBottom: 10,
  },
});

Verse.propTypes = {
  verseNum: PropTypes.string.isRequired,
  verseText: PropTypes.string.isRequired,
};

export default Verse;
