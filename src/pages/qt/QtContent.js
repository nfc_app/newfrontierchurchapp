import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, View, Clipboard, Text } from 'react-native';

import { qtIndexChange } from '@actions/qt';

import { COLOR_BACKGROUND, FONT_SIZE_XXSMALL } from '@styles/Common';
import NFCButton from '@shared_components/button/NFCButton';
import StackHeader from '@shared_components/header/StackHeader';

import VerseList from './components/VerseList';
import QtHeader from './components/QtHeader';

class QtContent extends React.Component {
  static headerMode = 'float';

  static navigationOptions = ({ navigation }) => ({
    header: <StackHeader navigation={navigation} />,
  });

  static onClick() {
    console.log('Copy verse onClick');
  }

  render() {
    const { uiText, qtOfTheDayInfo, verses, onNextClick, onPrevClick, preQt, nextQt } = this.props;
    return (
      <View style={styles.container}>
        <QtHeader
          qtOfTheDayInfo={qtOfTheDayInfo}
          prev={preQt}
          next={nextQt}
          onPrevClick={onPrevClick}
          onNextClick={onNextClick}
        />
        <ScrollView style={styles.scrollViewContainer}>
          <View style={styles.buttonViewContainer}>
            <VerseList qtInfo={qtOfTheDayInfo.qtTitle} verses={verses} />
            <NFCButton
              text={uiText.interface.copyVersesButton}
              onClick={() => {
                let verseStr = qtOfTheDayInfo.qtTitle + "\n\n";
                verses.map(verse => {
                  verseStr = verseStr + verse.verseNum + '. ' + verse.verseText + " \n";
                });
                Clipboard.setString(verseStr);
              }}
            />
            <Text style={styles.copyright}>{uiText.copyright}</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

QtContent.propTypes = {
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  qtOfTheDayInfo: PropTypes.shape({
    dayOfWeek: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    qtTitle: PropTypes.string.isRequired,
  }).isRequired,
  verses: PropTypes.arrayOf(
    PropTypes.shape({
      verseNum: PropTypes.string.isRequired,
      verseText: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
  onPrevClick: PropTypes.func.isRequired,
  onNextClick: PropTypes.func.isRequired,
  preQt: PropTypes.shape({
    dayOfWeek: PropTypes.string,
    date: PropTypes.string,
    qtTitle: PropTypes.string,
  }).isRequired,
  nextQt: PropTypes.shape({
    dayOfWeek: PropTypes.string,
    date: PropTypes.string,
    qtTitle: PropTypes.string,
  }).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollViewContainer: {
    width: '100%',
  },
  buttonViewContainer: {
    margin: 15,
    marginBottom: 20,
    alignItems: 'center',
  },
  copyright: {
    fontSize: FONT_SIZE_XXSMALL,
    fontStyle: 'italic',
  },
});

function getQtInfo(qt, lang, indexVariant) {
  if (qt.qtContent[lang].length < 1 || qt.selectionIndex < 0) {
    return {}
  }
  const idxVal = qt.selectionIndex + indexVariant;
  if (idxVal >= 0 && idxVal < qt.qtContent[lang].length) {
    return qt.qtContent[lang][idxVal].qtMetaData;
  }
  return {}
}

function getCurrentQtVerses(qt, lang) {
  if (qt.qtContent[lang].length < 1 || qt.selectionIndex < 0) {
    return '';
  }
  return qt.qtContent[lang][qt.selectionIndex].verses;
}

const mapStateToProps = state => ({
  uiText: state.uiText.qt,
  qtOfTheDayInfo: getQtInfo(state.qt, state.session.lang, 0),
  preQt: getQtInfo(state.qt, state.session.lang, -1),
  nextQt: getQtInfo(state.qt, state.session.lang, 1),
  verses: getCurrentQtVerses(state.qt, state.session.lang),
});

const mapDispatchToProps = dispatch => ({
  onPrevClick: () => dispatch(qtIndexChange(-1)),
  onNextClick: () => dispatch(qtIndexChange(1)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QtContent);
