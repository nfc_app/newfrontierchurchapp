import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, View, RefreshControl } from 'react-native';
import { Divider } from 'react-native-elements';

import { fetchFormsIfNeeded } from '@actions/forms';
import { SPACING_LARGE, COLOR_BACKGROUND, DIVIDER_HEIGHT } from '@styles/Common';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import TitleHeading from '@shared_components/title_heading/TitleHeading';

import ApplicationList from './components/ApplicationList';


class ApplicationPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentDidMount() {
    const { getForms } = this.props;
    getForms(false);
  }

  onRefresh = () => {
    const { getForms } = this.props;
    this.setState({ refreshing: true });
    getForms(true)
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch(() => {
        alert('Network error - could not fetch announcements!');
        this.setState({ refreshing: false });
      });
  };

  render() {
    const { lang, uiText, applications, navigation } = this.props;
    const { refreshing } = this.state;
    return (
      <View style={styles.container}>
        <DrawerHeader navigation={navigation} />
        <ScrollView
          style={styles.scrollContainer}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
        >
          <TitleHeading text={uiText.header} />
          <Divider style={styles.divider} />
          <ApplicationList applications={applications[lang]} />
        </ScrollView>
      </View>
    );
  }
}

ApplicationPage.propTypes = {
  lang: PropTypes.string.isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  applications: PropTypes.objectOf(PropTypes.any).isRequired,
  getForms: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  scrollContainer: {
    paddingBottom: SPACING_LARGE,
  },
  divider: {
    height: DIVIDER_HEIGHT,
  },
});

const mapStateToProps = state => ({
  lang: state.session.lang,
  uiText: state.uiText.forms,
  applications: state.forms.applicationFormInfos,
});

const mapDispatchToProps = dispatch => ({
  getForms: force => dispatch(fetchFormsIfNeeded(force)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationPage);
