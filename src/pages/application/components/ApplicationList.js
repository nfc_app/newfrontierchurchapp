import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Linking } from 'react-native';

import { COLOR_BACKGROUND } from '@styles/Common';
import ListRow from '@shared_components/row_item/ListRow';

function onClick(url) {
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      console.log(`Don't know how to open ${url}`);
    }
  });
}

const ApplicationList = ({ applications }) => (
  <View style={styles.container}>
    {applications && applications.map((application) => (
      <ListRow
        key={application.title}
        text={application.title}
        subtext={application.content}
        showArrow
        onPress={() => onClick(application.url)}
      />
    ))}
  </View>
);

ApplicationList.propTypes = {
  applications: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string.isRequired,
      content: PropTypes.string,
      url: PropTypes.string.isRequired,
    }).isRequired
  ),
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
});

export default ApplicationList;
