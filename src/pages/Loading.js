import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { AsyncStorage, Image, StyleSheet } from 'react-native';
import { persistStore } from 'redux-persist';
import { connect } from 'react-redux';

import LinearGradient from 'react-native-linear-gradient';

import { COLOR_PRIMARY, COLOR_PRIMARY_DARK } from '@styles/Common';
import { fetchVersionIfNeeded } from '@actions/version';
import { appConstants } from '@resources/Constants';

class Loading extends Component {
  constructor() {
    super();
    this.unsubscriber = null;
  }

  componentDidMount() {
    persistStore(this.props.screenProps, {storage: AsyncStorage}, () => {
      const { getVersion, navigation, version } = this.props;
      getVersion();

      const updateRequired = appConstants.version < version;
      const nextPage = updateRequired ? 'Update' : 'Main';
      navigation.navigate(nextPage);
    });
  }

  componentWillUnmount() {
    if (this.unsubscriber) {
      this.unsubscriber();
    }
  }

  render() {
    return (
      <LinearGradient colors={[COLOR_PRIMARY, COLOR_PRIMARY_DARK]} style={styles.container}>
        <Image source={LOGO_IMAGE} />
      </LinearGradient>
    );
  }
}

const LOGO_IMAGE = require('@assets/images/logo_home.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

Loading.defaultProps = {
  updateRequired: false,
}

Loading.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  getVersion: PropTypes.func.isRequired,
  version: PropTypes.string,
};

const mapStateToProps = state => ({
  version: state.version.number,
});

const mapDispatchToProps = dispatch => ({
  getVersion: () => dispatch(fetchVersionIfNeeded()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Loading);
