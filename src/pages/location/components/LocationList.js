import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { SPACING_SMALL, SPACING_MEDIUM, SPACING_XXXSMALL, COLOR_BACKGROUND, TEXT_LINE_HEIGHT, FONT_SIZE_XXSMALL } from '@styles/Common';

import ListRow from '@shared_components/row_item/ListRow';

function getDetail({time, location, context}) {
  return [time, location, context].filter(Boolean).join("\n");
}

const LocationList = ({ serviceInfos, disclaimer }) => (
  <View style={styles.container}>
    <ScrollView>
      {serviceInfos.map((serviceInfo) => (
        <ListRow
          key={serviceInfo.title}
          text={serviceInfo.title}
          subtext={getDetail(serviceInfo)}
          textStyle={styles.textStyle}
          subtextStyle={styles.subtextStyle}
        />
      ))}
    </ScrollView>
    {disclaimer && <Text style={styles.disclaimer}>{disclaimer}</Text>}
  </View>
);

LocationList.propTypes = {
  serviceInfos: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      time: PropTypes.string.isRequired,
      location: PropTypes.string.isRequired,
      context: PropTypes.string,
    }).isRequired
  ).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: COLOR_BACKGROUND,
  },
  textStyle: {
    paddingBottom: SPACING_XXXSMALL,
  },
  subtextStyle: {
    lineHeight: TEXT_LINE_HEIGHT,
  },
  disclaimer: {
    paddingVertical: SPACING_SMALL,
    paddingHorizontal: SPACING_MEDIUM,
    fontSize: FONT_SIZE_XXSMALL,
    fontStyle: 'italic',
  }
});

export default LocationList;
