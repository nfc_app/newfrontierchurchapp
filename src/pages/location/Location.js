import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';

import { COLOR_BACKGROUND } from '@styles/Common';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import TabBarView from '@shared_components/tab/TabBarView';

import LocationList from './components/LocationList';

const Location = props => {
  const { uiText, navigation, lang, sundayList, weekdayList } = props;
  const tabArray = [
    <LocationList key="sundayList" tabLabel={uiText.sundayHeader} serviceInfos={sundayList[lang]} disclaimer={uiText.disclaimer} />,
    <LocationList key="weekdayList" tabLabel={uiText.weekdayHeader} serviceInfos={weekdayList[lang]} />,
  ];

  return (
    <View style={styles.container}>
      <DrawerHeader navigation={navigation} />
      <TitleHeading text={uiText.header} />
      <TabBarView tabs={tabArray} />
    </View>
  );
};

Location.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  lang: PropTypes.string.isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  sundayList: PropTypes.objectOf(PropTypes.any).isRequired,
  weekdayList: PropTypes.objectOf(PropTypes.any).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
});

const mapStateToProps = state => ({
  uiText: state.uiText.location,
  lang: state.session.lang,
  sundayList: state.location.sundayServices,
  weekdayList: state.location.weekdayServices,
});

export default connect(mapStateToProps)(Location);
