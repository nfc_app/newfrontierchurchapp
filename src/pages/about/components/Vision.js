import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import {
  SPACING_XXSMALL,
  SPACING_XSMALL,
  SPACING_SMALL,
  COLOR_PRIMARY,
  COLOR_BACKGROUND,
} from '@styles/Common';

import SubtitleText from '@shared_components/text/SubtitleText';
import BodyText from '@shared_components/text/BodyText';

function getText(type, text) {
  switch (type) {
    case 'title':
      return <SubtitleText key={text} text={text} style={styles[type]} />;
    case 'subtitle':
      return <BodyText key={text} text={text} style={styles[type]} />;
    case 'body':
      return <BodyText key={text} text={text} style={styles[type]} />;
    default:
      return null;
  }
}

const Vision = props => {
  const { lang, vision } = props;
  return (
    <View style={styles.container}>{vision[lang].map(item => getText(item.type, item.text))}</View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
    marginHorizontal: SPACING_SMALL,
    paddingTop: SPACING_XXSMALL,
  },
  title: {
    marginVertical: SPACING_XXSMALL,
  },
  subtitle: {
    color: COLOR_PRIMARY,
  },
  body: {
    marginBottom: SPACING_XSMALL,
  },
});

Vision.propTypes = {
  lang: PropTypes.string.isRequired,
  vision: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  lang: state.session.lang,
  vision: state.about.vision,
});

export default connect(mapStateToProps)(Vision);
