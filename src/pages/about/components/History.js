import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import { SPACING_XXSMALL, SPACING_SMALL, COLOR_PRIMARY, COLOR_BACKGROUND } from '@styles/Common';

import BodyText from '@shared_components/text/BodyText';

const History = props => {
  const { lang, history } = props;
  return (
    <View style={styles.container}>
      {history[lang].map(item => (
        <Text key={item.body} style={styles.text}>
          <BodyText text={item.date} style={styles.date} />
          <BodyText text={item.body} />
        </Text>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
    marginHorizontal: SPACING_SMALL,
    paddingTop: SPACING_SMALL,
  },
  date: {
    color: COLOR_PRIMARY,
    marginBottom: SPACING_XXSMALL,
  },
  text: {
    marginBottom: SPACING_SMALL,
  },
});

History.propTypes = {
  lang: PropTypes.string.isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  lang: state.session.lang,
  history: state.about.history,
});

export default connect(mapStateToProps)(History);
