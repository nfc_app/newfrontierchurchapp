import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Image, StyleSheet, View } from 'react-native';
import {
  SPACING_SMALL,
  SPACING_MEDIUM,
  SPACING_XLARGE,
  COLOR_PRIMARY,
  COLOR_BLACK,
  COLOR_BACKGROUND,
  COLOR_GREY_DARKEST
} from '@styles/Common';

import BodyText from '@shared_components/text/BodyText';
import HyperlinkText from '@shared_components/text/HyperlinkText';

function renderImage(image) {
  if (image) {
    return <Image source={image} style={styles.image} />;
  } else {
    return <View style={[styles.image, {backgroundColor: COLOR_GREY_DARKEST}]} />
  }
}

function renderRow(image, name, description) {
  return (
    <View key={name} style={styles.rowContainer}>
      {renderImage(image)}
      <View style={styles.textContainer}>
        <BodyText text={name} style={styles.name} />
        {description && description.map(item => getItem(item))}
      </View>
    </View>
  );
}

function getItem({ type, text, image, name, description, url }) {
  switch (type) {
    case 'body':
      return <BodyText key={text} text={text} />;
    case 'link':
      return <HyperlinkText key={text} text={text} url={url} />;
    case 'row':
      return renderRow(image, name, description);
    default:
      return null;
  }
}

const Pastors = props => {
  const { lang, pastors } = props;
  return <View style={styles.container}>{pastors[lang].map(item => getItem(item))}</View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
    marginHorizontal: SPACING_SMALL,
    paddingTop: SPACING_SMALL,
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: SPACING_XLARGE,
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 50,
    marginRight: 30,
  },
  subtitle: {
    color: COLOR_BLACK,
    marginBottom: SPACING_MEDIUM,
  },
  name: {
    color: COLOR_PRIMARY,
  },
});

Pastors.propTypes = {
  lang: PropTypes.string.isRequired,
  pastors: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  lang: state.session.lang,
  pastors: state.about.pastors,
});

export default connect(mapStateToProps)(Pastors);
