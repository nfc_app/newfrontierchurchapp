import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import { SPACING_SMALL, SPACING_LARGE, COLOR_PRIMARY, COLOR_BACKGROUND } from '@styles/Common';

import BoldBodyText from '@shared_components/text/BoldBodyText';
import BodyText from '@shared_components/text/BodyText';
import HyperlinkText from '@shared_components/text/HyperlinkText';

const Leadership = props => {
  const { lang, leadership } = props;
  return (
    <View style={styles.container}>
      {leadership[lang].map((item, index) => (
        <Text key={index}>
          {item.bold && <BoldBodyText text={item.bold} />}
          {item.highlight && <BodyText text={item.highlight} style={styles.highlight} />}
          {item.description && <BodyText text={item.description} />}
          {item.email && <HyperlinkText url={item.url} text={item.email} />}
        </Text>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
    marginHorizontal: SPACING_SMALL,
    paddingBottom: SPACING_LARGE,
  },
  highlight: {
    color: COLOR_PRIMARY,
  },
});

Leadership.propTypes = {
  lang: PropTypes.string.isRequired,
  leadership: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  lang: state.session.lang,
  leadership: state.about.leadership,
});

export default connect(mapStateToProps)(Leadership);
