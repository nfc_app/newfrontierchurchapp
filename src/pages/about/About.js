import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, Image, View } from 'react-native';

import { COLOR_BACKGROUND } from '@styles/Common';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import TabBarView from '@shared_components/tab/TabBarView';

import Vision from './components/Vision';
import History from './components/History';
import Pastors from './components/Pastors';
import Leadership from './components/Leadership';

const IMAGE = require('@assets/images/about_group_photo.jpg');

const About = props => {
  const { uiText, navigation } = props;
  const collapsibleComponent = (
    <View style={styles.container}>
      <TitleHeading text={uiText.header} />
      <Image source={IMAGE} resizeMode="center" style={styles.image} />
    </View>
  );
  const tabArray = [
    <Vision key="vision" tabLabel={uiText.vision} />,
    <History key="history" tabLabel={uiText.history} />,
    <Pastors key="pastors" tabLabel={uiText.pastors} />,
    <Leadership key="leadership" tabLabel={uiText.leadership} />,
  ];

  return (
    <View style={styles.container}>
      <DrawerHeader navigation={navigation} />
      <TabBarView tabs={tabArray} collapsibleComponent={collapsibleComponent} />
    </View>
  );
};

About.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  image: {
    flex: 1,
    width: null,
    height: 160,
    resizeMode: 'cover',
    marginBottom: 20,
  },
});

const mapStateToProps = state => ({
  uiText: state.uiText.about,
});

export default connect(mapStateToProps)(About);
