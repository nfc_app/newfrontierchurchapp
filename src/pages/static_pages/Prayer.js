import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Linking, StyleSheet, ScrollView, View } from 'react-native';
import { COLOR_BACKGROUND } from '@styles/Common';
import StaticContent from '@shared_components/StaticContent';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import NFCButton from '@shared_components/button/NFCButton';

function onClick(link) {
  Linking.openURL(link);
}

const Prayer = props => {
  const { uiText, navigation } = props;
  return (
    <View style={styles.container}>
      <DrawerHeader navigation={navigation} />
      <ScrollView style={styles.container}>
        <TitleHeading text={uiText.header} />

        {uiText.bodyText.map(item => (
          <StaticContent key={item.title} title={item.title} body={item.body} />
        ))}

        <View style={styles.buttonContainer}>
          <NFCButton text={uiText.button} onClick={() => onClick(uiText.link)} />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  buttonContainer: {
    alignItems: 'center',
  },
});

Prayer.propTypes = {
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  uiText: state.uiText.prayer,
});

export default connect(mapStateToProps)(Prayer);
