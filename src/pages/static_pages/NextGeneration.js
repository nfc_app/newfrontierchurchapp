import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';

import { COLOR_BACKGROUND, SPACING_MEDIUM } from '@styles/Common';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import TabBarView from '@shared_components/tab/TabBarView';
import StaticContent from '@shared_components/StaticContent';

const NextGeneration = props => {
  const { uiText, navigation, lang, sunday, korea } = props;
  const tabArray = [
    <StaticContent key="sundayList" tabLabel={sunday[lang].title} body={sunday[lang].body} bodyStyle={styles.bodyStyle} disclaimer={sunday[lang].disclaimer} />,
    <StaticContent key="koreaList" tabLabel={korea[lang].title} body={korea[lang].body} bodyStyle={styles.bodyStyle} />,
  ];

  return (
    <View style={styles.container}>
      <DrawerHeader navigation={navigation} />
      <TitleHeading text={uiText.header} />
      <TabBarView tabs={tabArray} />
    </View>
  );
};

NextGeneration.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  lang: PropTypes.string.isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  sunday: PropTypes.objectOf(PropTypes.any).isRequired,
  korea: PropTypes.objectOf(PropTypes.any).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  bodyStyle: {
    marginTop: SPACING_MEDIUM
  }
});

const mapStateToProps = state => ({
  lang: state.session.lang,
  uiText: state.uiText.nextgen,
  sunday: state.nextgen.sunday,
  korea: state.nextgen.korea,
});

export default connect(mapStateToProps)(NextGeneration);
