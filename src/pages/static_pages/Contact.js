import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Linking, StyleSheet, ScrollView, View } from 'react-native';
import { COLOR_BACKGROUND } from '@styles/Common';
import StaticContent from '@shared_components/StaticContent';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import NFCButton from '@shared_components/button/NFCButton';

function onClick(url) {
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      console.log(`Don't know how to open ${url}`);
    }
  });
}

const Contact = props => {
  const { uiText, navigation } = props;
  return (
    <View style={styles.container}>
      <DrawerHeader navigation={navigation} />
      <ScrollView style={styles.container}>
        <TitleHeading text={uiText.header} />

        {uiText.bodyText.map(item => (
          <StaticContent key={item.body.charAt(0)} body={item.body} />
        ))}
        <View style={styles.buttonContainer}>
          <NFCButton text={uiText.interface.emailButton} onClick={() => onClick(uiText.interface.url)} />
        </View>
      </ScrollView>
    </View>
  );
};

Contact.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  buttonContainer: {
    alignItems: 'center',
  },
});

const mapStateToProps = state => ({
  uiText: state.uiText.contact,
});

export default connect(mapStateToProps)(Contact);
