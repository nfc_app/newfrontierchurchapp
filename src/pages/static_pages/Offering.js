import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, Text, View } from 'react-native';
import {
  COLOR_BACKGROUND,
  COLOR_PRIMARY,
  FONT_SIZE_XXSMALL,
  SPACING_LARGE,
  SPACING_SMALL,
  SPACING_XXSMALL,
} from '@styles/Common';

import TitleHeading from '@shared_components/title_heading/TitleHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import BodyText from '@shared_components/text/BodyText';
import BoldBodyText from '@shared_components/text/BoldBodyText';
import SubtitleText from '@shared_components/text/SubtitleText';
import HyperlinkText from '@shared_components/text/HyperlinkText';

function getText(type, text, url) {
  switch (type) {
    case 'title':
      return <SubtitleText key={text} text={text} style={styles[type]} />;
    case 'subtitle':
      return <BoldBodyText key={text} text={text} style={styles[type]} />;
    case 'body':
      return <BodyText key={text} text={text} style={styles[type]} />;
    case 'highlight':
      return <BodyText key={text} text={text} style={styles[type]} />;
    case 'link':
      return <HyperlinkText key={text} url={url} text={text} style={styles[type]} />;
    case 'footprint':
      return <BodyText key={text} text={text} style={styles[type]} />;
    default:
      return null;
  }
}

const Offering = props => {
  const { uiText, navigation } = props;
  console.log(uiText.bodyText);
  return (
    <View style={styles.container}>
      <DrawerHeader navigation={navigation} />
      <ScrollView>
        <TitleHeading text={uiText.header} />
        <Text style={styles.textContainer}>
          {uiText.bodyText.map(item => getText(item.type, item.text, item.url))}
        </Text>
      </ScrollView>
    </View>
  );
};

Offering.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  textContainer: {
    marginHorizontal: SPACING_SMALL,
    paddingBottom: SPACING_LARGE,
  },
  title: {
    margin: SPACING_XXSMALL,
  },
  highlight: {
    color: COLOR_PRIMARY,
  },
  link: {
    color: COLOR_PRIMARY,
    fontWeight: 'bold',
  },
  footprint: {
    fontSize: FONT_SIZE_XXSMALL,
  },
});

const mapStateToProps = state => ({
  uiText: state.uiText.offering,
});

export default connect(mapStateToProps)(Offering);
