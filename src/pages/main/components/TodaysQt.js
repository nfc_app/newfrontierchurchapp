import React from 'react';
import PropTypes from 'prop-types';

import TodayQtRow from './TodayQtRow';

function getTodaysQt(qtContent) {
  const dayOfWeekIndex = new Date().getDay();
  let dayOfWeek = '', date = '', title = '';
  if (qtContent.length > dayOfWeekIndex) {
    if (!qtContent[dayOfWeekIndex]) {
      title = 'QT Content Not Available';
    } else if (!qtContent[dayOfWeekIndex].qtMetaData) {
      title = "Unable to Retrieve Today's QT Content";
    } else {
      title = qtContent[dayOfWeekIndex].qtMetaData.qtTitle;
      date = qtContent[dayOfWeekIndex].qtMetaData.date;
      dayOfWeek = qtContent[dayOfWeekIndex].qtMetaData.dayOfWeek;
    }
  }
  return { dayOfWeek, date, title }
}

const TodaysQt = props => {
  const { qtContent, navigation, onClick } = props;
  return (
    <TodayQtRow
      qtInfo={getTodaysQt(qtContent)}
      onClick={onClick}
      navigation={navigation}
      type="QtContent"
    />
  );
};

TodaysQt.propTypes = {
  qtContent: PropTypes.arrayOf(PropTypes.object).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default TodaysQt;
