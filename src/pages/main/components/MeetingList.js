import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import ListRow from '@shared_components/row_item/ListRow';

const MeetingList = props => {
  const { list, lang } = props;
  return (
    <View>
      {list.map(meeting => (
        <ListRow
          key={meeting[lang].title}
          text={meeting[lang].title}
          subtext={meeting[lang].description}
        />
      ))}
    </View>
  );
};

MeetingList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  lang: PropTypes.string.isRequired,
};

export default MeetingList;
