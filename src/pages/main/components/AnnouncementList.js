import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import ExpandableListRow from '@shared_components/row_item/ExpandableListRow';

const AnnouncementList = props => {
  const { list, lang } = props;
  return (
    <View>
      {list.map(announcement => (
        <ExpandableListRow
          key={announcement[lang].title}
          title={announcement[lang].title}
          body={announcement[lang].content}
        />
      ))}
    </View>
  );
};

AnnouncementList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  lang: PropTypes.string.isRequired,
};

export default AnnouncementList;
