import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  COLOR_BACKGROUND,
  COLOR_GREY,
  SPACING_MEDIUM,
  SPACING_LARGE,
  DIVIDER_HEIGHT,
} from '@styles/Common';
import PropTypes from 'prop-types';

import BoldBodyText from '@shared_components/text/BoldBodyText';
import BodyText from '@shared_components/text/BodyText';

const TodayQtRow = props => {
  const { navigation, type, onClick, qtInfo } = props;

  return (
    <View style={styles.viewContainer}>
      <Text
        onPress={() => {
          onClick(new Date().getDay());
          navigation.navigate(type);
        }}
      >
        <BoldBodyText text={`${qtInfo.dayOfWeek} ${qtInfo.date} `} />
        <BodyText text={qtInfo.title} />
      </Text>
    </View>
  );
};

TodayQtRow.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  onClick: PropTypes.func.isRequired,
  qtInfo: PropTypes.shape({
    dayOfWeek: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }).isRequired,
  type: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  viewContainer: {
    backgroundColor: COLOR_BACKGROUND,
    paddingVertical: SPACING_LARGE,
    paddingHorizontal: SPACING_MEDIUM,
    borderBottomColor: COLOR_GREY,
    borderBottomWidth: DIVIDER_HEIGHT,
  },
});

export default TodayQtRow;
