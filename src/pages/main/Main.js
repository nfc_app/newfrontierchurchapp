import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, ScrollView, View, RefreshControl } from 'react-native';
import { COLOR_BACKGROUND, SPACING_MEDIUM, DIVIDER_HEIGHT } from '@styles/Common';
import { connect } from 'react-redux';

import { fetchAnnouncementIfNeeded } from '@actions/announcement';
import { qtIndexSet, fetchQtsIfNeeded } from '@actions/qt';
import { fetchMeetingIfNeeded } from '@actions/meeting';

import TitleHeading from '@shared_components/title_heading/TitleHeading';
import ListHeading from '@shared_components/title_heading/ListHeading';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import BodyText from '@shared_components/text/BodyText';

import TodaysQt from './components/TodaysQt';
import AnnouncementList from './components/AnnouncementList';
import MeetingList from './components/MeetingList';
import { Divider } from 'react-native-elements';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentDidMount() {
    const { getQt, getAnnouncements, getMeetings } = this.props;
    getQt(false);
    getAnnouncements(false);
    getMeetings(false);
  }

  onRefresh = () => {
    const { getQt, getAnnouncements, getMeetings } = this.props;
    this.setState({ refreshing: true });
    getQt(true)
      .then(() => {
        getAnnouncements(true);
        getMeetings(true);
        this.setState({ refreshing: false });
      })
      .catch(() => {
        alert('Network error - could not refresh!');
        this.setState({ refreshing: false });
      });
  };

  render() {
    const {
      lang,
      uiText,
      qtContent,
      announcementList,
      meetingList,
      navigation,
      onClick,
    } = this.props;
    const { refreshing } = this.state;
    const isKorean = lang === 'ko';
    return (
      <View style={styles.container}>
        <DrawerHeader navigation={navigation} />
        <ScrollView
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
        >
          <TitleHeading text={uiText.main.header} />

          <Divider style={styles.divider} />
          <ListHeading text={uiText.qt.header} />
          <TodaysQt qtContent={qtContent} navigation={navigation} onClick={onClick} />

          <ListHeading text={uiText.announcement.header} />
          {isKorean ? (
            <AnnouncementList lang={lang} list={announcementList} />
          ) : (
            <BodyText
              text={uiText.announcement.hardcodedAnnoucements}
              style={styles.announcement}
            />
          )}

          {isKorean && meetingList.length > 0 && <ListHeading text={uiText.meeting.header} />}
          {isKorean && meetingList.length > 0  && <MeetingList lang={lang} list={meetingList} />}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  divider: {
    height: DIVIDER_HEIGHT,
  },
  announcement: {
    margin: SPACING_MEDIUM,
  },
});

Main.defaultProps = {
  lang: 'ko',
  qtContent: [],
  announcementList: [],
  meetingList: [],
};

Main.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  lang: PropTypes.string,
  getQt: PropTypes.func.isRequired,
  getAnnouncements: PropTypes.func.isRequired,
  getMeetings: PropTypes.func.isRequired,
  qtContent: PropTypes.arrayOf(PropTypes.object),
  announcementList: PropTypes.arrayOf(PropTypes.object),
  meetingList: PropTypes.arrayOf(PropTypes.object),
  onClick: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  lang: state.session.lang,
  uiText: state.uiText,
  qtContent: state.qt.qtContent[state.session.lang],
  announcementList: state.announcement.list,
  meetingList: state.meeting.list,
});

const mapDispatchToProps = dispatch => ({
  getQt: force => dispatch(fetchQtsIfNeeded(force)),
  getAnnouncements: force => dispatch(fetchAnnouncementIfNeeded(force)),
  getMeetings: force => dispatch(fetchMeetingIfNeeded(force)),
  onClick: value => {
    console.log(`QT index selected to value ${value}`);
    dispatch(qtIndexSet(value));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
