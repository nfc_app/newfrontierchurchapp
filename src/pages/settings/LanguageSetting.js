import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Divider } from 'react-native-elements';

import { COLOR_BACKGROUND, DIVIDER_HEIGHT } from '@styles/Common';
import StackHeader from '@shared_components/header/StackHeader';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import RadioButtonRow from '@shared_components/row_item/RadioButtonRow';

import { sessionSetLanguage } from '@store/actions/session';

class LanguageSetting extends React.Component {
  static headerMode = 'float';

  static navigationOptions = ({ navigation }) => ({
    header: <StackHeader navigation={navigation} />,
  });

  render() {
    const { uiText, lang, setLanguage } = this.props;
    return (
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.container}>
          <TitleHeading text={uiText.header} />
          <Divider style={styles.divider} />
          {Object.keys(uiText.options).map(key => (
            <RadioButtonRow
              key={key}
              text={uiText.options[key]}
              checked={lang === key}
              onPress={() => setLanguage(key)}
            />
          ))}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
    alignItems: 'center',
  },
  container: {
    width: '100%',
  },
  divider: {
    height: DIVIDER_HEIGHT,
  },
});

LanguageSetting.propTypes = {
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  setLanguage: PropTypes.func.isRequired,
  lang: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  uiText: state.uiText.languageSetting,
  lang: state.session.lang,
});

const mapDispatchToProps = dispatch => ({
  setLanguage: lang => dispatch(sessionSetLanguage(lang)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LanguageSetting);
