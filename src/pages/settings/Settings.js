import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Divider } from 'react-native-elements';

import { COLOR_BACKGROUND, DIVIDER_HEIGHT } from '@styles/Common';

import DrawerHeader from '@shared_components/header/DrawerHeader';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import ListRow from '@shared_components/row_item/ListRow';
// import NFCButton from '@shared_components/button/NFCButton';

import { currentUserSetUser } from '@store/actions/currentUser';

class Settings extends React.Component {
  static headerMode = 'float';

  static navigationOptions = ({ navigation }) => ({
    header: <DrawerHeader navigation={navigation} />,
  });

  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    const { navigation } = this.props;
    navigation.navigate('LanguageSetting');
  }

  logout() {
    const { navigation, setCurrentUser } = this.props;
    setCurrentUser(null);
  }

  render() {
    const { uiText } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          <TitleHeading text={uiText.header} />
          <Divider style={styles.divider} />
          <ListRow text={uiText.menu.language} onPress={this.onPress} showArrow />
          {/* <NFCButton text={uiText.interface.logOutButton} onClick={this.logout} fullWidth={false} /> */}
        </ScrollView>
      </View>
    );
  }
}

Settings.propTypes = {
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  setCurrentUser: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
  divider: {
    height: DIVIDER_HEIGHT,
  },
});

const mapStateToProps = state => ({
  uiText: state.uiText.settings,
  user: state.currentUser.currentUser,
});

const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(currentUserSetUser(user)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);
