import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { COLOR_BACKGROUND } from '@styles/Common';

import SermonEntry from './SermonEntry';

const SermonList = ({ sermons }) => (
  <View style={styles.container}>
    {sermons && sermons.map((sermon, index) => (
      <SermonEntry key={index} sermonInfo={sermon} />
    ))}
  </View>
);

SermonList.propTypes = {
  sermons: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      who: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
    }).isRequired
  ),
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND,
  },
});

export default SermonList;
