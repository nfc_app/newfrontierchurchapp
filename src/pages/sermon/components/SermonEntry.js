import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Linking } from 'react-native';

import ListRow from '@shared_components/row_item/ListRow';

function onClick(url) {
  Linking
    .canOpenURL(url)
    .then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log(`Don't know how to open ${url}`);
      }
  });
}

function getDetail({date, type, who}) {
  const info = "by: " + who;
  return [info, type, date].join("\n");
}

const SermonEntry = ({ uiText, sermonInfo }) => (
  <ListRow
    key={sermonInfo.title}
    text={sermonInfo.title}
    subtext={getDetail(sermonInfo, uiText)}
    onPress={() => onClick(sermonInfo.url)}
    showArrow
  />
);

SermonEntry.propTypes = {
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  sermonInfo: PropTypes.shape({
    date: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    who: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = state => ({
  uiText: state.uiText.sermon,
});

export default connect(mapStateToProps)(SermonEntry);
