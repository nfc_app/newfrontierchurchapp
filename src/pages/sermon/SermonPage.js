import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, StyleSheet, ScrollView, RefreshControl } from 'react-native';
import { Divider } from 'react-native-elements';

import { SPACING_LARGE, DIVIDER_HEIGHT } from '@styles/Common';
import { fetchSermonsIfNeeded } from '@actions/sermons';
import DrawerHeader from '@shared_components/header/DrawerHeader';
import TitleHeading from '@shared_components/title_heading/TitleHeading';
import SermonList from "./components/SermonList";

class SermonPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentDidMount() {
    const { getSermonList } = this.props;
    getSermonList(false);
  }

  onRefresh = () => {
    const { getSermonList } = this.props;
    this.setState({ refreshing: true });
    getSermonList(true)
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch(() => {
        alert('Network error - could not fetch sermon!');
        this.setState({ refreshing: false });
      });
  };

  render() {
    const { lang, uiText, sermons, navigation } = this.props;
    const { refreshing } = this.state;
    const sermonList = sermons[lang];
    return (
      <View style={styles.container}>
        <DrawerHeader navigation={navigation} />
        <ScrollView
          style={styles.scrollContainer}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
        >
          <TitleHeading text={uiText.header} />
          <Divider style={styles.divider} />
          <SermonList sermons={sermonList} />
        </ScrollView>
      </View>
    );
  }
}

SermonPage.propTypes = {
  lang: PropTypes.string.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  uiText: PropTypes.objectOf(PropTypes.any).isRequired,
  sermons: PropTypes.objectOf(PropTypes.any).isRequired,
  getSermonList: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollContainer: {
    marginBottom: SPACING_LARGE,
  },
  divider: {
    height: DIVIDER_HEIGHT,
  },
});

const getSermonInfos = sermons => sermons.sermonInfos;

const mapStateToProps = state => ({
  lang: state.session.lang,
  uiText: state.uiText.sermon,
  sermons: getSermonInfos(state.sermons),
});

const mapDispatchToProps = dispatch => ({
  getSermonList: force => dispatch(fetchSermonsIfNeeded(force)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SermonPage);
