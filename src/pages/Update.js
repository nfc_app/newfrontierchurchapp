import React from 'react';
import { Linking, Platform, View, StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import TitleText from '@shared_components/text/TitleText';
import BodyText from '@shared_components/text/BodyText';
import NFCButton from '@shared_components/button/NFCButton';
import { SPACING_SMALL } from '@styles/Common';

function onClick() {
  const url = Platform.OS === 'ios'
    ? "https://itunes.apple.com/us/app/new-frontier-church/id513851549?mt=8"
    : "https://play.google.com/store/apps/details?id=com.nfc.newfrontierchurchapp";
  Linking.openURL(url);
}

const Update = () => {
  return (
    <View style={styles.container}>
      <TitleText text={"Update required"} />
      <BodyText style={styles.bodyStyle} text={"There is a newer version of the app available. Please update in order to use the app."} />
      <View style={styles.buttonContainer}>
        <NFCButton text={"Update"} onClick={onClick} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: SPACING_SMALL,
    height: getStatusBarHeight(true) + 50,
    paddingTop: getStatusBarHeight(true),
  },
  buttonContainer: {
    alignItems: 'center',
  }
});

export default Update;