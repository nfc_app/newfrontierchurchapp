import { Platform } from 'react-native';

// we define available font weight and styles for each font here
const font = {
  NanumSquareOTF: {
    weights: {
      ExtraBold: '800',
      Light: '300',
      Regular: '400',
    },
    styles: {
      Italic: 'italic',
    },
  },
};

// generate styles for a font with given weight and style
const fontMaker = (options = {}) => {
  let { weight, style, family } = Object.assign(
    {
      weight: null,
      style: null,
      family: 'NanumSquareOTF',
    },
    options
  );

  const { weights, styles } = font[family];

  if (Platform.OS === 'android') {
    weight = weights[weight] ? weight : '';
    style = styles[style] ? style : '';

    const suffix = weight + style;

    return {
      fontFamily: family + (suffix.length ? `-${suffix}` : ''),
    };
  }
  weight = weights[weight] || weights.Normal;
  style = styles[style] || 'normal';

  return {
    fontFamily: family,
    fontWeight: weight,
    fontStyle: style,
  };
};

export default fontMaker;
