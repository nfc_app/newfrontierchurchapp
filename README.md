### Start the app

- 1) Start the react native packager, run `yarn run start` or `npm start` from the root of your project.
- 2) **[iOS]** Build and run the iOS app, run `react-native run-ios` from the root of your project. The first build will take some time. This will automatically start up a simulator also for you on a successful build if one wasn't already started.
- 3) **[Android]** If you haven't already got an android device attached/emulator running then you'll need to get one running (make sure the emulator is with Google Play / APIs). When ready run `react-native run-android` from the root of your project.

### Troubleshooting

- If you cannot build from XCode, try opening `node_modules/react-native/React/React.xcodeproj`, building it, and then cleaning and rebuilding the project on XCode.
- If you have issues with fsevent when doing an npm install, try downgrading node to 8. (I had issues with node 11.)